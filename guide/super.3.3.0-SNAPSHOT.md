# super.3.3.0-SNAPSHOT

## 1、新增 mime 类型配置

- `MIME`(Multipurpose Internet Mail Extensions)多用途互联网邮件扩展类型。是设定某种扩展名的文件用一种应用程序来打开的方式类型。在万维网中使用的 HTTP 协议也使用了 MIME 的框架，标准被扩展为互联网媒体类型。

- 业务上需要用到 `spring-boot` 的静态资源访问功能，静态资源目录(例如 `static` )里存放了非常见的文件类型，该文件类型没有被 `spring` 记录 `media type`，所有无法在响应头 `content-type` 正确返回其类型。

- 借鉴 spring 处理 media type 的方式，isass 通过解析 `src/main/resources/META-INF/mime.type` 文件，把 mime 添加到 spring 相关的 bean, 让 spring 能正确认识新文件类型。

### 1.1、配置方式

- 在项目的代码目录 `src/main/resources/META-INF/` 添加 utf-8 编码的文件 `mime.type`。本例添加了 `pbf` `geojson` 两种文件格式对应的 mime type

``` text
# this media types will be register into spring context using ContentNegotiationConfigurer
#
# MIME type (lowercased)			Extensions(space separated)
# ======================================================
application/x-protobuf		  pbf
application/json              geojson
```

> 注：
> - 第一列为 `mime type`
> - 第二列开始为 `文件扩展名`
> - 同一行可以写多个文件扩展名，用空格分开

### 1.2、使用方式

#### 1.2.1、访问静态资源文件

springboot 默认将资源目录下的 `static` 和 `public` 目录作为静态资源目录，前端能直接访问该目录下的文件。响应头 content-type 会被 spring 处理并返回，开发者无需处理。

#### 1.2.2、使用代码获取文件的 mime type

- 1：先用 org.springframework.http.MediaTypeFactory.getMediaType() 方法获取 spring 内置支持的 mime 类型。注意方法的参数是 `filename`，需要包含 `.` 的扩展名才能被解析。 
- 2：若第一步获取不到，则需要注入 spring 已有的 bean `contentNegotiationManager`, 调用其方法获取指定文件扩展名的 media type。此 bean 包含了我们在 mime.type 配置的内容。

``` java

@Resource
private ContentNegotiationManager contentNegotiationManager;

MediaType mediaType = MediaTypeFactory.getMediaType("xxx.pbf");
if (mediaType == null) {
    mediaType = contentNegotiationManager.getMediaTypeMappings().get("pbf");
}

```

## 2、接口返回自定义时间字段格式

v2 系列通用查询接口中，新增查询参数，支持指定返回字段的时间格式化。

- api 文档展示 advanceFeature 高级特性字段

<img :src="$withBase('/img/advance-feature-date-format.jpg')" alt="advance-feature-date-format.jpg">

- 请求效果

<img :src="$withBase('/img/advance-feature-date-format-request.png')" alt="advance-feature-date-format-request.png">

## 3、接口返回自定义小数位数格式

v2 系列通用查询接口中，新增查询参数，支持指定返回字段的小数保留位数格式化。

- api 文档展示 advanceFeature 高级特性字段

<img :src="$withBase('/img/advance-feature-decimal-places.jpg')" alt="advance-feature-decimal-places.jpg">

- 请求效果

<img :src="$withBase('/img/advance-feature-decimal-places-request.png')" alt="advance-feature-decimal-places-request.png">

> 注意
>
> 在 query 传递 json 需要 endocing
<img :src="$withBase('/img/advance-feature-json-encoding.jpg')" alt="advance-feature-json-encoding.jpg">
