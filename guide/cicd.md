# 持续集成/持续交付

> 本文将介绍微服务的持续集成/持续交付流程

::: tip 涉及工具
git仓库、jenkins、nexus（maven私服/jar包仓库）、harbor（容器镜像仓库）、k8s（容器编排工具）
:::

## 相关概念

- 开发分支：用于保存开发阶段的代码分支。

- 发布分支：用于发布正式版本的代码分支。

- 快照版本：以 `-SNAPSHOT` 结尾，例 `1.0.0-SNAPSHOT` `2.3.4-SNAPSHOT` 。

- 正式版本：不能以 `-SNAPSHOT` 结尾，例 `1.0.0` `2.3.4` 。

- git标签：每个正式版本均会打上版本标签，便于以后能找到对应版本的代码

## 约束

### 开发分支

- 程序员提交代码到此分支时，应保证代码能正常编译通过，避免影响他人开发。

- 此分支的代码，未经正式测试，不能对外发布版本。

- pom文件版本应为快照版本。

### 发布分支

- 此分支原则上从 `开发分支` 或其他紧急修复 `bugfix` 分支合并而来，不能直接提交代码到此分支。

- 此分支的代码，需经过正式测试后，对外发布版本。

- pom文件版本应为正式版本。

## 服务关系图
<iframe id="embed_dom" name="embed_dom" frameborder="0" style="display:block;width:600px; height:300px;" src="https://www.processon.com/embed/6322f32c637689341d5cdd20"></iframe>

## 流程图
<iframe id="embed_dom" name="embed_dom" frameborder="0" style="display:block;width:600px; height:300px;" src="https://www.processon.com/embed/606c35147d9c0829db719751"></iframe>

## 流程图补充说明

### '是否需要部署'节点

- 编译代码后是否需要部署服务。

- 默认的判断依据：当前分支为开发分支，则可以部署。

- 可根据公司/项目情况，修改jenkins脚本，调整判断逻辑。

### '获取部署环境'节点

- 获取部署到目标环境的信息，例如k8s的 ip、端口、token等

- 可按照快照版本、发布版本部署到不同的集群环境

- 可根据公司/项目情况，修改jenkins脚本，调整获取逻辑。


### '通知部署结果'节点

- jenkins编译部署流程完成后，调用第3方IM工具(如钉钉，企业微信)，发送部署结果，便于程序员及时方便了解部署情况

## java项目采用CI/CD流程

- 在 isass 微服务体系中（vip.isass.super版本>=super.3.0.6-SNAPSHOT），项目无需额外引入依赖。框架中已经依赖了 `maven-release-plugin` 插件

- 微服务项目中的 `{company}-service-{ms}-springcloud/jenkinsfile` 文件，将最后一行脚本调用代码 由 `JavaJenkinsfile.call(projectConfig)` 改为 `MavenJenkinsfile.call(projectConfig)`