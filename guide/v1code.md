# v1代码

在代码中，有很多 v1 前缀的代码，v1是标识这些代码文件原则上不能修改，因为我们在改表结构后，需要生成新的代码，直接覆盖。非 v1 部分，因为可能写了很多业务逻辑，所以需要人工对比。

## 生成的v1文件

共有5个 v1 文件
- V1Controller.java 接入层
- V1Service.java 业务逻辑层
- V1Repository.java 数据仓库接口层
- V1Mapper.java 数据源实现（mybatis-plus实现）
- V1Mapper.xml 数据源实现（mybatis-plus实现）