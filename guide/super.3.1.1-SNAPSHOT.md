# super.3.1.1-SNAPSHOT

## v2系列接口

### 通用增删改查接口重新定义
    
之前 service 层是独立接口的，service 增加了方法，只能在 service 代码层面应用，现在往更高层次抽象，把

- 入口层（例如 spring 的 controller，spring webflux(规划)、grpc(规划)、dubbo(规划)，mq的消费者(规划)）、
- 微服务调用层 feign、
- 缓存层 cache、
- 本地服务层 service

都统一实现IV2Service，统一各层的通用方法定义，减低开发者的理解与调用成本。

***
类图：

![isass v2 类图](http://assets.processon.com/chart_image/60f13300e0b34d0e1b68a69e.png)

### 取消v1独立包

取消现有v1包与自定义业务包的分离设计（controller、service等），减少生成的文件。

### feign代码生成

新增自动生成微服务之间调用的代码，不用人工写。生成代码即可支持全部通用接口的微服务间调用。

### 重构Criteria

取消查询条件实体的成员变量定义，数据直接保存到父类的V2WhereCondition列表，节省criteria实体的大量元数据内存占用，提高序列化与反序列化性能。

### V2Controller 强制返回 Resp 对象

::: tip 设计目的

1：能够把 `controller` 的返回值，与 `IV2Service` 定义的通用方法对齐，为 `controller` 纳入统一接口标准提供基础条件；

2：在开发  `controller` 接口时，部分开发者，没有遵循 [api响应数据格式约定规范](api-data-format.html)，随意返回 `Map` 对象，破坏统一性。

:::

触发强制转换条件：

- 实现了 `IV2Controller` 接口
- 方法有返回值，即方法定义的返回值关键字不是 `void`
- 返回值不是 `org.springframework.http.ResponseEntity`

## swagger展示优化

### jsonNode对象的swagger展示优化

原本 jsonNode 类型，在 swagger 中会展示其内部的所有变量，导致文档中展示20+个无意义字段，现在只展示花括号。

<img :src="$withBase('/img/jsonnode-swagger.png')" alt="jsonnode-swagger">

### 查询条件展示优化

原本swagger文档将criteria的上百个条件展示出来，导致调试和查看返回结果困难。现在只现在“等于”的字段。

<img :src="$withBase('/img/swagger-criteria.jpg')" alt="swagger-criteria">

## 取消独立api项目

在isass各微服务中，新建api项目，把 isass-api 的内容迁移到微服务中，删除isass-api内容。

## 权限安全

### urlAccessSecurityStrategy

- 删除原有 `security.enable` 配置
- 添加 `security.urlAccessSecurityStrategy` 配置

::: tip 配置说明

- 默认为：NONE

- NONE：不开启权限管理，所有请求都能访问（除了某些接口，代码用到登陆用户的信息）

- AUTHENTICATED：登录认证成功后，才能访问接口（除了代码指定不用token的接口（例如登录接口），其他接口都需要token才能访问）

- ROLE：基于角色认证的权限管理，需要在权限前端配置访问每个接口所需要的权限角色，每个用户需要赋予对应的角色"

:::

# super.3.1.1-SNAPSHOT