# 查询条件criteria

为了更方便地接收查询类型接口的前端入参，isass 为每个库表对应的实体对象，生成一个 criteria 对象。isass 的 orm 框架使用 [mybatis-plus(mp)](https://mybatis.plus)，criteria 最终会在数据层转换成 mp 的 wrapper。

## 作用

- 作为查询类型接口的入参，接收前端的查询参数

- 在经过业务层代码逻辑后，自动转换成 orm 框架所需代码实现，由 orm 执行数据库查询操作

## 优点

- 面向对象编程，而不是接收 Map 参数。使用 map 作为入参，开发一时爽，重构火葬场

- 若更换 orm 框架，业务代码无需变动

- 数据库字段变更时，自动生成代码，替换 criteria 文件即可使用新的查询条件

## 字段类型

| 字段类型 | 示例字段 | 字段名 |
|---|---|---|
| 全部类型 | 年龄: age | age<br> orAge<br> ageNotEqual<br> orAgeNotEqual<br> ageIn<br> orAgeIn<br> ageNotIn<br> orAgeNotIn |
| 数字类型 | 年龄: age | ageLessThan<br> orAgeLessThan<br> ageLessThanEqual<br> orAgeLessThanEqual<br> ageGreaterThan<br> orAgeGreaterThan<br> ageGreaterThanEqual<br> orAgeGreaterThanEqual |
| 字符串类型 | 名称：name | nameLike<br> orNameLike<br> nameNotLike<br> orNameNotLike<br> nameStartWith<br> orNameStartWith |
| 集合类型 | 标签：tags | tagsContainsAll<br> tagsContainsAny |

## 示例

短信 查询条件

``` java

package vip.isass.message.api.model.criteria;

import cn.hutool.core.collection.CollUtil;
import lombok.Getter;
import vip.isass.message.api.model.entity.Sms;
import vip.isass.core.criteria.IdCriteria;
import vip.isass.core.entity.Json;

import java.time.LocalDateTime;
import java.util.Collection;

/**
 * 短信 查询条件
 *
 * @author isass
 */
@Getter
public class SmsCriteria extends IdCriteria<SmsCriteria, Sms, String> {

    //************************************************** bizId **************************************************//

    private String bizId;

    private String orBizId;

    private String bizIdNotEqual;

    private String orBizIdNotEqual;

    private Collection<String> bizIdIn;

    private Collection<String> orBizIdIn;

    private Collection<String> bizIdNotIn;

    private Collection<String> orBizIdNotIn;

    private String bizIdLike;

    private String orBizIdLike;

    private String bizIdNotLike;

    private String orBizIdNotLike;

    private String bizIdStartWith;

    private String orBizIdStartWith;

    //************************************************** supplierResponse **************************************************//

    private Json supplierResponse;

    private Json orSupplierResponse;

    private Json supplierResponseNotEqual;

    private Json orSupplierResponseNotEqual;

    private Collection<Json> supplierResponseIn;

    private Collection<Json> orSupplierResponseIn;

    private Collection<Json> supplierResponseNotIn;

    private Collection<Json> orSupplierResponseNotIn;

    //************************************************** status **************************************************//

    private Integer status;

    private Integer orStatus;

    private Integer statusNotEqual;

    private Integer orStatusNotEqual;

    private Collection<Integer> statusIn;

    private Collection<Integer> orStatusIn;

    private Collection<Integer> statusNotIn;

    private Collection<Integer> orStatusNotIn;

    private Integer statusLessThan;

    private Integer orStatusLessThan;

    private Integer statusLessThanEqual;

    private Integer orStatusLessThanEqual;

    private Integer statusGreaterThan;

    private Integer orStatusGreaterThan;

    private Integer statusGreaterThanEqual;

    private Integer orStatusGreaterThanEqual;

    //************************************************** deleteFlag **************************************************//

    private Boolean deleteFlag;

    private Boolean orDeleteFlag;

    private Boolean deleteFlagNotEqual;

    private Boolean orDeleteFlagNotEqual;

    private Collection<Boolean> deleteFlagIn;

    private Collection<Boolean> orDeleteFlagIn;

    private Collection<Boolean> deleteFlagNotIn;

    private Collection<Boolean> orDeleteFlagNotIn;

    //************************************************** createUserId **************************************************//

    private String createUserId;

    private String orCreateUserId;

    private String createUserIdNotEqual;

    private String orCreateUserIdNotEqual;

    private Collection<String> createUserIdIn;

    private Collection<String> orCreateUserIdIn;

    private Collection<String> createUserIdNotIn;

    private Collection<String> orCreateUserIdNotIn;

    private String createUserIdLike;

    private String orCreateUserIdLike;

    private String createUserIdNotLike;

    private String orCreateUserIdNotLike;

    private String createUserIdStartWith;

    private String orCreateUserIdStartWith;

    //************************************************** createUserName **************************************************//

    private String createUserName;

    private String orCreateUserName;

    private String createUserNameNotEqual;

    private String orCreateUserNameNotEqual;

    private Collection<String> createUserNameIn;

    private Collection<String> orCreateUserNameIn;

    private Collection<String> createUserNameNotIn;

    private Collection<String> orCreateUserNameNotIn;

    private String createUserNameLike;

    private String orCreateUserNameLike;

    private String createUserNameNotLike;

    private String orCreateUserNameNotLike;

    private String createUserNameStartWith;

    private String orCreateUserNameStartWith;

    //************************************************** createTime **************************************************//

    private LocalDateTime createTime;

    private LocalDateTime orCreateTime;

    private LocalDateTime createTimeNotEqual;

    private LocalDateTime orCreateTimeNotEqual;

    private Collection<LocalDateTime> createTimeIn;

    private Collection<LocalDateTime> orCreateTimeIn;

    private Collection<LocalDateTime> createTimeNotIn;

    private Collection<LocalDateTime> orCreateTimeNotIn;

    private LocalDateTime createTimeLessThan;

    private LocalDateTime orCreateTimeLessThan;

    private LocalDateTime createTimeLessThanEqual;

    private LocalDateTime orCreateTimeLessThanEqual;

    private LocalDateTime createTimeGreaterThan;

    private LocalDateTime orCreateTimeGreaterThan;

    private LocalDateTime createTimeGreaterThanEqual;

    private LocalDateTime orCreateTimeGreaterThanEqual;

    //************************************************** modifyUserId **************************************************//

    private String modifyUserId;

    private String orModifyUserId;

    private String modifyUserIdNotEqual;

    private String orModifyUserIdNotEqual;

    private Collection<String> modifyUserIdIn;

    private Collection<String> orModifyUserIdIn;

    private Collection<String> modifyUserIdNotIn;

    private Collection<String> orModifyUserIdNotIn;

    private String modifyUserIdLike;

    private String orModifyUserIdLike;

    private String modifyUserIdNotLike;

    private String orModifyUserIdNotLike;

    private String modifyUserIdStartWith;

    private String orModifyUserIdStartWith;

    //************************************************** modifyUserName **************************************************//

    private String modifyUserName;

    private String orModifyUserName;

    private String modifyUserNameNotEqual;

    private String orModifyUserNameNotEqual;

    private Collection<String> modifyUserNameIn;

    private Collection<String> orModifyUserNameIn;

    private Collection<String> modifyUserNameNotIn;

    private Collection<String> orModifyUserNameNotIn;

    private String modifyUserNameLike;

    private String orModifyUserNameLike;

    private String modifyUserNameNotLike;

    private String orModifyUserNameNotLike;

    private String modifyUserNameStartWith;

    private String orModifyUserNameStartWith;

    //************************************************** modifyTime **************************************************//

    private LocalDateTime modifyTime;

    private LocalDateTime orModifyTime;

    private LocalDateTime modifyTimeNotEqual;

    private LocalDateTime orModifyTimeNotEqual;

    private Collection<LocalDateTime> modifyTimeIn;

    private Collection<LocalDateTime> orModifyTimeIn;

    private Collection<LocalDateTime> modifyTimeNotIn;

    private Collection<LocalDateTime> orModifyTimeNotIn;

    private LocalDateTime modifyTimeLessThan;

    private LocalDateTime orModifyTimeLessThan;

    private LocalDateTime modifyTimeLessThanEqual;

    private LocalDateTime orModifyTimeLessThanEqual;

    private LocalDateTime modifyTimeGreaterThan;

    private LocalDateTime orModifyTimeGreaterThan;

    private LocalDateTime modifyTimeGreaterThanEqual;

    private LocalDateTime orModifyTimeGreaterThanEqual;

    //************************************************** receivingMobiles **************************************************//

    private Collection<String> receivingMobiles;

    private Collection<String> orReceivingMobiles;

    private Collection<String> receivingMobilesNotEqual;

    private Collection<String> orReceivingMobilesNotEqual;

    private Collection<Collection<String>> receivingMobilesIn;

    private Collection<Collection<String>> orReceivingMobilesIn;

    private Collection<Collection<String>> receivingMobilesNotIn;

    private Collection<Collection<String>> orReceivingMobilesNotIn;

    private Collection<String> receivingMobilesContains;

    //************************************************** sendTime **************************************************//

    private LocalDateTime sendTime;

    private LocalDateTime orSendTime;

    private LocalDateTime sendTimeNotEqual;

    private LocalDateTime orSendTimeNotEqual;

    private Collection<LocalDateTime> sendTimeIn;

    private Collection<LocalDateTime> orSendTimeIn;

    private Collection<LocalDateTime> sendTimeNotIn;

    private Collection<LocalDateTime> orSendTimeNotIn;

    private LocalDateTime sendTimeLessThan;

    private LocalDateTime orSendTimeLessThan;

    private LocalDateTime sendTimeLessThanEqual;

    private LocalDateTime orSendTimeLessThanEqual;

    private LocalDateTime sendTimeGreaterThan;

    private LocalDateTime orSendTimeGreaterThan;

    private LocalDateTime sendTimeGreaterThanEqual;

    private LocalDateTime orSendTimeGreaterThanEqual;

    //************************************************** content **************************************************//

    private String content;

    private String orContent;

    private String contentNotEqual;

    private String orContentNotEqual;

    private Collection<String> contentIn;

    private Collection<String> orContentIn;

    private Collection<String> contentNotIn;

    private Collection<String> orContentNotIn;

    private String contentLike;

    private String orContentLike;

    private String contentNotLike;

    private String orContentNotLike;

    private String contentStartWith;

    private String orContentStartWith;

    //************************************************** bizType **************************************************//

    private Integer bizType;

    private Integer orBizType;

    private Integer bizTypeNotEqual;

    private Integer orBizTypeNotEqual;

    private Collection<Integer> bizTypeIn;

    private Collection<Integer> orBizTypeIn;

    private Collection<Integer> bizTypeNotIn;

    private Collection<Integer> orBizTypeNotIn;

    private Integer bizTypeLessThan;

    private Integer orBizTypeLessThan;

    private Integer bizTypeLessThanEqual;

    private Integer orBizTypeLessThanEqual;

    private Integer bizTypeGreaterThan;

    private Integer orBizTypeGreaterThan;

    private Integer bizTypeGreaterThanEqual;

    private Integer orBizTypeGreaterThanEqual;

    //************************************************** sendCount **************************************************//

    private Integer sendCount;

    private Integer orSendCount;

    private Integer sendCountNotEqual;

    private Integer orSendCountNotEqual;

    private Collection<Integer> sendCountIn;

    private Collection<Integer> orSendCountIn;

    private Collection<Integer> sendCountNotIn;

    private Collection<Integer> orSendCountNotIn;

    private Integer sendCountLessThan;

    private Integer orSendCountLessThan;

    private Integer sendCountLessThanEqual;

    private Integer orSendCountLessThanEqual;

    private Integer sendCountGreaterThan;

    private Integer orSendCountGreaterThan;

    private Integer sendCountGreaterThanEqual;

    private Integer orSendCountGreaterThanEqual;

    //************************************************** bizId setter **************************************************//

    public SmsCriteria setBizId(String bizId) {
        this.bizId = bizId;
        equals(Sms.BIZ_ID, this.bizId);
        return this;
    }

    public SmsCriteria setOrBizId(String bizId) {
        this.orBizId = bizId;
        orEquals(Sms.BIZ_ID, this.orBizId);
        return this;
    }

    public SmsCriteria setBizIdNotEqual(String bizIdNotEqual) {
        this.bizIdNotEqual = bizIdNotEqual;
        notEquals(Sms.BIZ_ID, this.bizIdNotEqual);
        return this;
    }

    public SmsCriteria setOrBizIdNotEqual(String orBizIdNotEqual) {
        this.orBizIdNotEqual = orBizIdNotEqual;
        orNotEquals(Sms.BIZ_ID, this.orBizIdNotEqual);
        return this;
    }

    public SmsCriteria setBizIdIn(Collection<String> bizIds) {
        this.bizIdIn = bizIds;
        in(Sms.BIZ_ID, this.bizIdIn);
        return this;
    }

    public SmsCriteria setOrBizIdIn(Collection<String> bizIds) {
        this.orBizIdIn = bizIds;
        orIn(Sms.BIZ_ID, this.orBizIdIn);
        return this;
    }

    public SmsCriteria setBizIdNotIn(Collection<String> bizIds) {
        this.bizIdNotIn = bizIds;
        notIn(Sms.BIZ_ID, this.bizIdNotIn);
        return this;
    }

    public SmsCriteria setOrBizIdNotIn(Collection<String> bizIds) {
        this.orBizIdNotIn = bizIds;
        orNotIn(Sms.BIZ_ID, this.orBizIdNotIn);
        return this;
    }

    public SmsCriteria setBizIdIn(String... bizIds) {
        this.bizIdIn = CollUtil.newHashSet(bizIds);
        in(Sms.BIZ_ID, this.bizIdIn);
        return this;
    }

    public SmsCriteria setOrBizIdIn(String... bizIds) {
        this.orBizIdIn = CollUtil.newHashSet(bizIds);
        orIn(Sms.BIZ_ID, this.orBizIdIn);
        return this;
    }

    public SmsCriteria setBizIdNotIn(String... bizIds) {
        this.bizIdNotIn = CollUtil.newHashSet(bizIds);
        notIn(Sms.BIZ_ID, this.bizIdNotIn);
        return this;
    }

    public SmsCriteria setOrBizIdNotIn(String... bizIds) {
        this.orBizIdNotIn = CollUtil.newHashSet(bizIds);
        orNotIn(Sms.BIZ_ID, this.orBizIdNotIn);
        return this;
    }

    public SmsCriteria setBizIdLike(String bizIdLike) {
        this.bizIdLike = bizIdLike == null ? null : bizIdLike.trim();
        like(Sms.BIZ_ID, this.bizIdLike);
        return this;
    }

    public SmsCriteria setOrBizIdLike(String orBizIdLike) {
        this.orBizIdLike = orBizIdLike == null ? null : orBizIdLike.trim();
        orLike(Sms.BIZ_ID, this.orBizIdLike);
        return this;
    }

    public SmsCriteria setBizIdNotLike(String bizIdNotLike) {
        this.bizIdNotLike = bizIdNotLike == null ? null : bizIdNotLike.trim();
        notLike(Sms.BIZ_ID, this.bizIdNotLike);
        return this;
    }

    public SmsCriteria setOrBizIdNotLike(String orBizIdNotLike) {
        this.orBizIdNotLike = orBizIdNotLike == null ? null : orBizIdNotLike.trim();
        orNotLike(Sms.BIZ_ID, this.orBizIdNotLike);
        return this;
    }

    public SmsCriteria setBizIdStartWith(String bizIdStartWith) {
        this.bizIdStartWith = bizIdStartWith == null ? null : bizIdStartWith.trim();
        startWith(Sms.BIZ_ID, bizIdStartWith);
        return this;
    }

    public SmsCriteria setOrBizIdStartWith(String orBizIdStartWith) {
        this.orBizIdStartWith = orBizIdStartWith == null ? null : orBizIdStartWith.trim();
        orStartWith(Sms.BIZ_ID, orBizIdStartWith);
        return this;
    }

    //************************************************** supplierResponse setter **************************************************//

    public SmsCriteria setSupplierResponse(Json supplierResponse) {
        this.supplierResponse = supplierResponse;
        equals(Sms.SUPPLIER_RESPONSE, this.supplierResponse);
        return this;
    }

    public SmsCriteria setOrSupplierResponse(Json supplierResponse) {
        this.orSupplierResponse = supplierResponse;
        orEquals(Sms.SUPPLIER_RESPONSE, this.orSupplierResponse);
        return this;
    }

    public SmsCriteria setSupplierResponseNotEqual(Json supplierResponseNotEqual) {
        this.supplierResponseNotEqual = supplierResponseNotEqual;
        notEquals(Sms.SUPPLIER_RESPONSE, this.supplierResponseNotEqual);
        return this;
    }

    public SmsCriteria setOrSupplierResponseNotEqual(Json orSupplierResponseNotEqual) {
        this.orSupplierResponseNotEqual = orSupplierResponseNotEqual;
        orNotEquals(Sms.SUPPLIER_RESPONSE, this.orSupplierResponseNotEqual);
        return this;
    }

    public SmsCriteria setSupplierResponseIn(Collection<Json> supplierResponses) {
        this.supplierResponseIn = supplierResponses;
        in(Sms.SUPPLIER_RESPONSE, this.supplierResponseIn);
        return this;
    }

    public SmsCriteria setOrSupplierResponseIn(Collection<Json> supplierResponses) {
        this.orSupplierResponseIn = supplierResponses;
        orIn(Sms.SUPPLIER_RESPONSE, this.orSupplierResponseIn);
        return this;
    }

    public SmsCriteria setSupplierResponseNotIn(Collection<Json> supplierResponses) {
        this.supplierResponseNotIn = supplierResponses;
        notIn(Sms.SUPPLIER_RESPONSE, this.supplierResponseNotIn);
        return this;
    }

    public SmsCriteria setOrSupplierResponseNotIn(Collection<Json> supplierResponses) {
        this.orSupplierResponseNotIn = supplierResponses;
        orNotIn(Sms.SUPPLIER_RESPONSE, this.orSupplierResponseNotIn);
        return this;
    }

    public SmsCriteria setSupplierResponseIn(Json... supplierResponses) {
        this.supplierResponseIn = CollUtil.newHashSet(supplierResponses);
        in(Sms.SUPPLIER_RESPONSE, this.supplierResponseIn);
        return this;
    }

    public SmsCriteria setOrSupplierResponseIn(Json... supplierResponses) {
        this.orSupplierResponseIn = CollUtil.newHashSet(supplierResponses);
        orIn(Sms.SUPPLIER_RESPONSE, this.orSupplierResponseIn);
        return this;
    }

    public SmsCriteria setSupplierResponseNotIn(Json... supplierResponses) {
        this.supplierResponseNotIn = CollUtil.newHashSet(supplierResponses);
        notIn(Sms.SUPPLIER_RESPONSE, this.supplierResponseNotIn);
        return this;
    }

    public SmsCriteria setOrSupplierResponseNotIn(Json... supplierResponses) {
        this.orSupplierResponseNotIn = CollUtil.newHashSet(supplierResponses);
        orNotIn(Sms.SUPPLIER_RESPONSE, this.orSupplierResponseNotIn);
        return this;
    }

    //************************************************** status setter **************************************************//

    public SmsCriteria setStatus(Integer status) {
        this.status = status;
        equals(Sms.STATUS, this.status);
        return this;
    }

    public SmsCriteria setOrStatus(Integer status) {
        this.orStatus = status;
        orEquals(Sms.STATUS, this.orStatus);
        return this;
    }

    public SmsCriteria setStatusNotEqual(Integer statusNotEqual) {
        this.statusNotEqual = statusNotEqual;
        notEquals(Sms.STATUS, this.statusNotEqual);
        return this;
    }

    public SmsCriteria setOrStatusNotEqual(Integer orStatusNotEqual) {
        this.orStatusNotEqual = orStatusNotEqual;
        orNotEquals(Sms.STATUS, this.orStatusNotEqual);
        return this;
    }

    public SmsCriteria setStatusIn(Collection<Integer> statuss) {
        this.statusIn = statuss;
        in(Sms.STATUS, this.statusIn);
        return this;
    }

    public SmsCriteria setOrStatusIn(Collection<Integer> statuss) {
        this.orStatusIn = statuss;
        orIn(Sms.STATUS, this.orStatusIn);
        return this;
    }

    public SmsCriteria setStatusNotIn(Collection<Integer> statuss) {
        this.statusNotIn = statuss;
        notIn(Sms.STATUS, this.statusNotIn);
        return this;
    }

    public SmsCriteria setOrStatusNotIn(Collection<Integer> statuss) {
        this.orStatusNotIn = statuss;
        orNotIn(Sms.STATUS, this.orStatusNotIn);
        return this;
    }

    public SmsCriteria setStatusIn(Integer... statuss) {
        this.statusIn = CollUtil.newHashSet(statuss);
        in(Sms.STATUS, this.statusIn);
        return this;
    }

    public SmsCriteria setOrStatusIn(Integer... statuss) {
        this.orStatusIn = CollUtil.newHashSet(statuss);
        orIn(Sms.STATUS, this.orStatusIn);
        return this;
    }

    public SmsCriteria setStatusNotIn(Integer... statuss) {
        this.statusNotIn = CollUtil.newHashSet(statuss);
        notIn(Sms.STATUS, this.statusNotIn);
        return this;
    }

    public SmsCriteria setOrStatusNotIn(Integer... statuss) {
        this.orStatusNotIn = CollUtil.newHashSet(statuss);
        orNotIn(Sms.STATUS, this.orStatusNotIn);
        return this;
    }

    public SmsCriteria setStatusLessThan(Integer statusLessThan) {
        this.statusLessThan = statusLessThan;
        lessThan(Sms.STATUS, this.statusLessThan);
        return this;
    }

    public SmsCriteria setOrStatusLessThan(Integer orStatusLessThan) {
        this.orStatusLessThan = orStatusLessThan;
        orLessThan(Sms.STATUS, this.orStatusLessThan);
        return this;
    }

    public SmsCriteria setStatusLessThanEqual(Integer statusLessThanEqual) {
        this.statusLessThanEqual = statusLessThanEqual;
        lessThanEqual(Sms.STATUS, this.statusLessThanEqual);
        return this;
    }

    public SmsCriteria setOrStatusLessThanEqual(Integer orStatusLessThanEqual) {
        this.orStatusLessThanEqual = orStatusLessThanEqual;
        orLessThanEqual(Sms.STATUS, this.orStatusLessThanEqual);
        return this;
    }

    public SmsCriteria setStatusGreaterThan(Integer statusGreaterThan) {
        this.statusGreaterThan = statusGreaterThan;
        greaterThan(Sms.STATUS, this.statusGreaterThan);
        return this;
    }

    public SmsCriteria setOrStatusGreaterThan(Integer orStatusGreaterThan) {
        this.orStatusGreaterThan = orStatusGreaterThan;
        orGreaterThan(Sms.STATUS, this.orStatusGreaterThan);
        return this;
    }

    public SmsCriteria setStatusGreaterThanEqual(Integer statusGreaterThanEqual) {
        this.statusGreaterThanEqual = statusGreaterThanEqual;
        greaterThanEqual(Sms.STATUS, this.statusGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrStatusGreaterThanEqual(Integer orStatusGreaterThanEqual) {
        this.orStatusGreaterThanEqual = orStatusGreaterThanEqual;
        orGreaterThanEqual(Sms.STATUS, this.orStatusGreaterThanEqual);
        return this;
    }

    //************************************************** deleteFlag setter **************************************************//

    public SmsCriteria setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
        equals(Sms.DELETE_FLAG, this.deleteFlag);
        return this;
    }

    public SmsCriteria setOrDeleteFlag(Boolean deleteFlag) {
        this.orDeleteFlag = deleteFlag;
        orEquals(Sms.DELETE_FLAG, this.orDeleteFlag);
        return this;
    }

    public SmsCriteria setDeleteFlagNotEqual(Boolean deleteFlagNotEqual) {
        this.deleteFlagNotEqual = deleteFlagNotEqual;
        notEquals(Sms.DELETE_FLAG, this.deleteFlagNotEqual);
        return this;
    }

    public SmsCriteria setOrDeleteFlagNotEqual(Boolean orDeleteFlagNotEqual) {
        this.orDeleteFlagNotEqual = orDeleteFlagNotEqual;
        orNotEquals(Sms.DELETE_FLAG, this.orDeleteFlagNotEqual);
        return this;
    }

    public SmsCriteria setDeleteFlagIn(Collection<Boolean> deleteFlags) {
        this.deleteFlagIn = deleteFlags;
        in(Sms.DELETE_FLAG, this.deleteFlagIn);
        return this;
    }

    public SmsCriteria setOrDeleteFlagIn(Collection<Boolean> deleteFlags) {
        this.orDeleteFlagIn = deleteFlags;
        orIn(Sms.DELETE_FLAG, this.orDeleteFlagIn);
        return this;
    }

    public SmsCriteria setDeleteFlagNotIn(Collection<Boolean> deleteFlags) {
        this.deleteFlagNotIn = deleteFlags;
        notIn(Sms.DELETE_FLAG, this.deleteFlagNotIn);
        return this;
    }

    public SmsCriteria setOrDeleteFlagNotIn(Collection<Boolean> deleteFlags) {
        this.orDeleteFlagNotIn = deleteFlags;
        orNotIn(Sms.DELETE_FLAG, this.orDeleteFlagNotIn);
        return this;
    }

    public SmsCriteria setDeleteFlagIn(Boolean... deleteFlags) {
        this.deleteFlagIn = CollUtil.newHashSet(deleteFlags);
        in(Sms.DELETE_FLAG, this.deleteFlagIn);
        return this;
    }

    public SmsCriteria setOrDeleteFlagIn(Boolean... deleteFlags) {
        this.orDeleteFlagIn = CollUtil.newHashSet(deleteFlags);
        orIn(Sms.DELETE_FLAG, this.orDeleteFlagIn);
        return this;
    }

    public SmsCriteria setDeleteFlagNotIn(Boolean... deleteFlags) {
        this.deleteFlagNotIn = CollUtil.newHashSet(deleteFlags);
        notIn(Sms.DELETE_FLAG, this.deleteFlagNotIn);
        return this;
    }

    public SmsCriteria setOrDeleteFlagNotIn(Boolean... deleteFlags) {
        this.orDeleteFlagNotIn = CollUtil.newHashSet(deleteFlags);
        orNotIn(Sms.DELETE_FLAG, this.orDeleteFlagNotIn);
        return this;
    }

    //************************************************** createUserId setter **************************************************//

    public SmsCriteria setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
        equals(Sms.CREATE_USER_ID, this.createUserId);
        return this;
    }

    public SmsCriteria setOrCreateUserId(String createUserId) {
        this.orCreateUserId = createUserId;
        orEquals(Sms.CREATE_USER_ID, this.orCreateUserId);
        return this;
    }

    public SmsCriteria setCreateUserIdNotEqual(String createUserIdNotEqual) {
        this.createUserIdNotEqual = createUserIdNotEqual;
        notEquals(Sms.CREATE_USER_ID, this.createUserIdNotEqual);
        return this;
    }

    public SmsCriteria setOrCreateUserIdNotEqual(String orCreateUserIdNotEqual) {
        this.orCreateUserIdNotEqual = orCreateUserIdNotEqual;
        orNotEquals(Sms.CREATE_USER_ID, this.orCreateUserIdNotEqual);
        return this;
    }

    public SmsCriteria setCreateUserIdIn(Collection<String> createUserIds) {
        this.createUserIdIn = createUserIds;
        in(Sms.CREATE_USER_ID, this.createUserIdIn);
        return this;
    }

    public SmsCriteria setOrCreateUserIdIn(Collection<String> createUserIds) {
        this.orCreateUserIdIn = createUserIds;
        orIn(Sms.CREATE_USER_ID, this.orCreateUserIdIn);
        return this;
    }

    public SmsCriteria setCreateUserIdNotIn(Collection<String> createUserIds) {
        this.createUserIdNotIn = createUserIds;
        notIn(Sms.CREATE_USER_ID, this.createUserIdNotIn);
        return this;
    }

    public SmsCriteria setOrCreateUserIdNotIn(Collection<String> createUserIds) {
        this.orCreateUserIdNotIn = createUserIds;
        orNotIn(Sms.CREATE_USER_ID, this.orCreateUserIdNotIn);
        return this;
    }

    public SmsCriteria setCreateUserIdIn(String... createUserIds) {
        this.createUserIdIn = CollUtil.newHashSet(createUserIds);
        in(Sms.CREATE_USER_ID, this.createUserIdIn);
        return this;
    }

    public SmsCriteria setOrCreateUserIdIn(String... createUserIds) {
        this.orCreateUserIdIn = CollUtil.newHashSet(createUserIds);
        orIn(Sms.CREATE_USER_ID, this.orCreateUserIdIn);
        return this;
    }

    public SmsCriteria setCreateUserIdNotIn(String... createUserIds) {
        this.createUserIdNotIn = CollUtil.newHashSet(createUserIds);
        notIn(Sms.CREATE_USER_ID, this.createUserIdNotIn);
        return this;
    }

    public SmsCriteria setOrCreateUserIdNotIn(String... createUserIds) {
        this.orCreateUserIdNotIn = CollUtil.newHashSet(createUserIds);
        orNotIn(Sms.CREATE_USER_ID, this.orCreateUserIdNotIn);
        return this;
    }

    public SmsCriteria setCreateUserIdLike(String createUserIdLike) {
        this.createUserIdLike = createUserIdLike == null ? null : createUserIdLike.trim();
        like(Sms.CREATE_USER_ID, this.createUserIdLike);
        return this;
    }

    public SmsCriteria setOrCreateUserIdLike(String orCreateUserIdLike) {
        this.orCreateUserIdLike = orCreateUserIdLike == null ? null : orCreateUserIdLike.trim();
        orLike(Sms.CREATE_USER_ID, this.orCreateUserIdLike);
        return this;
    }

    public SmsCriteria setCreateUserIdNotLike(String createUserIdNotLike) {
        this.createUserIdNotLike = createUserIdNotLike == null ? null : createUserIdNotLike.trim();
        notLike(Sms.CREATE_USER_ID, this.createUserIdNotLike);
        return this;
    }

    public SmsCriteria setOrCreateUserIdNotLike(String orCreateUserIdNotLike) {
        this.orCreateUserIdNotLike = orCreateUserIdNotLike == null ? null : orCreateUserIdNotLike.trim();
        orNotLike(Sms.CREATE_USER_ID, this.orCreateUserIdNotLike);
        return this;
    }

    public SmsCriteria setCreateUserIdStartWith(String createUserIdStartWith) {
        this.createUserIdStartWith = createUserIdStartWith == null ? null : createUserIdStartWith.trim();
        startWith(Sms.CREATE_USER_ID, createUserIdStartWith);
        return this;
    }

    public SmsCriteria setOrCreateUserIdStartWith(String orCreateUserIdStartWith) {
        this.orCreateUserIdStartWith = orCreateUserIdStartWith == null ? null : orCreateUserIdStartWith.trim();
        orStartWith(Sms.CREATE_USER_ID, orCreateUserIdStartWith);
        return this;
    }

    //************************************************** createUserName setter **************************************************//

    public SmsCriteria setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
        equals(Sms.CREATE_USER_NAME, this.createUserName);
        return this;
    }

    public SmsCriteria setOrCreateUserName(String createUserName) {
        this.orCreateUserName = createUserName;
        orEquals(Sms.CREATE_USER_NAME, this.orCreateUserName);
        return this;
    }

    public SmsCriteria setCreateUserNameNotEqual(String createUserNameNotEqual) {
        this.createUserNameNotEqual = createUserNameNotEqual;
        notEquals(Sms.CREATE_USER_NAME, this.createUserNameNotEqual);
        return this;
    }

    public SmsCriteria setOrCreateUserNameNotEqual(String orCreateUserNameNotEqual) {
        this.orCreateUserNameNotEqual = orCreateUserNameNotEqual;
        orNotEquals(Sms.CREATE_USER_NAME, this.orCreateUserNameNotEqual);
        return this;
    }

    public SmsCriteria setCreateUserNameIn(Collection<String> createUserNames) {
        this.createUserNameIn = createUserNames;
        in(Sms.CREATE_USER_NAME, this.createUserNameIn);
        return this;
    }

    public SmsCriteria setOrCreateUserNameIn(Collection<String> createUserNames) {
        this.orCreateUserNameIn = createUserNames;
        orIn(Sms.CREATE_USER_NAME, this.orCreateUserNameIn);
        return this;
    }

    public SmsCriteria setCreateUserNameNotIn(Collection<String> createUserNames) {
        this.createUserNameNotIn = createUserNames;
        notIn(Sms.CREATE_USER_NAME, this.createUserNameNotIn);
        return this;
    }

    public SmsCriteria setOrCreateUserNameNotIn(Collection<String> createUserNames) {
        this.orCreateUserNameNotIn = createUserNames;
        orNotIn(Sms.CREATE_USER_NAME, this.orCreateUserNameNotIn);
        return this;
    }

    public SmsCriteria setCreateUserNameIn(String... createUserNames) {
        this.createUserNameIn = CollUtil.newHashSet(createUserNames);
        in(Sms.CREATE_USER_NAME, this.createUserNameIn);
        return this;
    }

    public SmsCriteria setOrCreateUserNameIn(String... createUserNames) {
        this.orCreateUserNameIn = CollUtil.newHashSet(createUserNames);
        orIn(Sms.CREATE_USER_NAME, this.orCreateUserNameIn);
        return this;
    }

    public SmsCriteria setCreateUserNameNotIn(String... createUserNames) {
        this.createUserNameNotIn = CollUtil.newHashSet(createUserNames);
        notIn(Sms.CREATE_USER_NAME, this.createUserNameNotIn);
        return this;
    }

    public SmsCriteria setOrCreateUserNameNotIn(String... createUserNames) {
        this.orCreateUserNameNotIn = CollUtil.newHashSet(createUserNames);
        orNotIn(Sms.CREATE_USER_NAME, this.orCreateUserNameNotIn);
        return this;
    }

    public SmsCriteria setCreateUserNameLike(String createUserNameLike) {
        this.createUserNameLike = createUserNameLike == null ? null : createUserNameLike.trim();
        like(Sms.CREATE_USER_NAME, this.createUserNameLike);
        return this;
    }

    public SmsCriteria setOrCreateUserNameLike(String orCreateUserNameLike) {
        this.orCreateUserNameLike = orCreateUserNameLike == null ? null : orCreateUserNameLike.trim();
        orLike(Sms.CREATE_USER_NAME, this.orCreateUserNameLike);
        return this;
    }

    public SmsCriteria setCreateUserNameNotLike(String createUserNameNotLike) {
        this.createUserNameNotLike = createUserNameNotLike == null ? null : createUserNameNotLike.trim();
        notLike(Sms.CREATE_USER_NAME, this.createUserNameNotLike);
        return this;
    }

    public SmsCriteria setOrCreateUserNameNotLike(String orCreateUserNameNotLike) {
        this.orCreateUserNameNotLike = orCreateUserNameNotLike == null ? null : orCreateUserNameNotLike.trim();
        orNotLike(Sms.CREATE_USER_NAME, this.orCreateUserNameNotLike);
        return this;
    }

    public SmsCriteria setCreateUserNameStartWith(String createUserNameStartWith) {
        this.createUserNameStartWith = createUserNameStartWith == null ? null : createUserNameStartWith.trim();
        startWith(Sms.CREATE_USER_NAME, createUserNameStartWith);
        return this;
    }

    public SmsCriteria setOrCreateUserNameStartWith(String orCreateUserNameStartWith) {
        this.orCreateUserNameStartWith = orCreateUserNameStartWith == null ? null : orCreateUserNameStartWith.trim();
        orStartWith(Sms.CREATE_USER_NAME, orCreateUserNameStartWith);
        return this;
    }

    //************************************************** createTime setter **************************************************//

    public SmsCriteria setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
        equals(Sms.CREATE_TIME, this.createTime);
        return this;
    }

    public SmsCriteria setOrCreateTime(LocalDateTime createTime) {
        this.orCreateTime = createTime;
        orEquals(Sms.CREATE_TIME, this.orCreateTime);
        return this;
    }

    public SmsCriteria setCreateTimeNotEqual(LocalDateTime createTimeNotEqual) {
        this.createTimeNotEqual = createTimeNotEqual;
        notEquals(Sms.CREATE_TIME, this.createTimeNotEqual);
        return this;
    }

    public SmsCriteria setOrCreateTimeNotEqual(LocalDateTime orCreateTimeNotEqual) {
        this.orCreateTimeNotEqual = orCreateTimeNotEqual;
        orNotEquals(Sms.CREATE_TIME, this.orCreateTimeNotEqual);
        return this;
    }

    public SmsCriteria setCreateTimeIn(Collection<LocalDateTime> createTimes) {
        this.createTimeIn = createTimes;
        in(Sms.CREATE_TIME, this.createTimeIn);
        return this;
    }

    public SmsCriteria setOrCreateTimeIn(Collection<LocalDateTime> createTimes) {
        this.orCreateTimeIn = createTimes;
        orIn(Sms.CREATE_TIME, this.orCreateTimeIn);
        return this;
    }

    public SmsCriteria setCreateTimeNotIn(Collection<LocalDateTime> createTimes) {
        this.createTimeNotIn = createTimes;
        notIn(Sms.CREATE_TIME, this.createTimeNotIn);
        return this;
    }

    public SmsCriteria setOrCreateTimeNotIn(Collection<LocalDateTime> createTimes) {
        this.orCreateTimeNotIn = createTimes;
        orNotIn(Sms.CREATE_TIME, this.orCreateTimeNotIn);
        return this;
    }

    public SmsCriteria setCreateTimeIn(LocalDateTime... createTimes) {
        this.createTimeIn = CollUtil.newHashSet(createTimes);
        in(Sms.CREATE_TIME, this.createTimeIn);
        return this;
    }

    public SmsCriteria setOrCreateTimeIn(LocalDateTime... createTimes) {
        this.orCreateTimeIn = CollUtil.newHashSet(createTimes);
        orIn(Sms.CREATE_TIME, this.orCreateTimeIn);
        return this;
    }

    public SmsCriteria setCreateTimeNotIn(LocalDateTime... createTimes) {
        this.createTimeNotIn = CollUtil.newHashSet(createTimes);
        notIn(Sms.CREATE_TIME, this.createTimeNotIn);
        return this;
    }

    public SmsCriteria setOrCreateTimeNotIn(LocalDateTime... createTimes) {
        this.orCreateTimeNotIn = CollUtil.newHashSet(createTimes);
        orNotIn(Sms.CREATE_TIME, this.orCreateTimeNotIn);
        return this;
    }

    public SmsCriteria setCreateTimeLessThan(LocalDateTime createTimeLessThan) {
        this.createTimeLessThan = createTimeLessThan;
        lessThan(Sms.CREATE_TIME, this.createTimeLessThan);
        return this;
    }

    public SmsCriteria setOrCreateTimeLessThan(LocalDateTime orCreateTimeLessThan) {
        this.orCreateTimeLessThan = orCreateTimeLessThan;
        orLessThan(Sms.CREATE_TIME, this.orCreateTimeLessThan);
        return this;
    }

    public SmsCriteria setCreateTimeLessThanEqual(LocalDateTime createTimeLessThanEqual) {
        this.createTimeLessThanEqual = createTimeLessThanEqual;
        lessThanEqual(Sms.CREATE_TIME, this.createTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setOrCreateTimeLessThanEqual(LocalDateTime orCreateTimeLessThanEqual) {
        this.orCreateTimeLessThanEqual = orCreateTimeLessThanEqual;
        orLessThanEqual(Sms.CREATE_TIME, this.orCreateTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setCreateTimeGreaterThan(LocalDateTime createTimeGreaterThan) {
        this.createTimeGreaterThan = createTimeGreaterThan;
        greaterThan(Sms.CREATE_TIME, this.createTimeGreaterThan);
        return this;
    }

    public SmsCriteria setOrCreateTimeGreaterThan(LocalDateTime orCreateTimeGreaterThan) {
        this.orCreateTimeGreaterThan = orCreateTimeGreaterThan;
        orGreaterThan(Sms.CREATE_TIME, this.orCreateTimeGreaterThan);
        return this;
    }

    public SmsCriteria setCreateTimeGreaterThanEqual(LocalDateTime createTimeGreaterThanEqual) {
        this.createTimeGreaterThanEqual = createTimeGreaterThanEqual;
        greaterThanEqual(Sms.CREATE_TIME, this.createTimeGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrCreateTimeGreaterThanEqual(LocalDateTime orCreateTimeGreaterThanEqual) {
        this.orCreateTimeGreaterThanEqual = orCreateTimeGreaterThanEqual;
        orGreaterThanEqual(Sms.CREATE_TIME, this.orCreateTimeGreaterThanEqual);
        return this;
    }

    //************************************************** modifyUserId setter **************************************************//

    public SmsCriteria setModifyUserId(String modifyUserId) {
        this.modifyUserId = modifyUserId;
        equals(Sms.MODIFY_USER_ID, this.modifyUserId);
        return this;
    }

    public SmsCriteria setOrModifyUserId(String modifyUserId) {
        this.orModifyUserId = modifyUserId;
        orEquals(Sms.MODIFY_USER_ID, this.orModifyUserId);
        return this;
    }

    public SmsCriteria setModifyUserIdNotEqual(String modifyUserIdNotEqual) {
        this.modifyUserIdNotEqual = modifyUserIdNotEqual;
        notEquals(Sms.MODIFY_USER_ID, this.modifyUserIdNotEqual);
        return this;
    }

    public SmsCriteria setOrModifyUserIdNotEqual(String orModifyUserIdNotEqual) {
        this.orModifyUserIdNotEqual = orModifyUserIdNotEqual;
        orNotEquals(Sms.MODIFY_USER_ID, this.orModifyUserIdNotEqual);
        return this;
    }

    public SmsCriteria setModifyUserIdIn(Collection<String> modifyUserIds) {
        this.modifyUserIdIn = modifyUserIds;
        in(Sms.MODIFY_USER_ID, this.modifyUserIdIn);
        return this;
    }

    public SmsCriteria setOrModifyUserIdIn(Collection<String> modifyUserIds) {
        this.orModifyUserIdIn = modifyUserIds;
        orIn(Sms.MODIFY_USER_ID, this.orModifyUserIdIn);
        return this;
    }

    public SmsCriteria setModifyUserIdNotIn(Collection<String> modifyUserIds) {
        this.modifyUserIdNotIn = modifyUserIds;
        notIn(Sms.MODIFY_USER_ID, this.modifyUserIdNotIn);
        return this;
    }

    public SmsCriteria setOrModifyUserIdNotIn(Collection<String> modifyUserIds) {
        this.orModifyUserIdNotIn = modifyUserIds;
        orNotIn(Sms.MODIFY_USER_ID, this.orModifyUserIdNotIn);
        return this;
    }

    public SmsCriteria setModifyUserIdIn(String... modifyUserIds) {
        this.modifyUserIdIn = CollUtil.newHashSet(modifyUserIds);
        in(Sms.MODIFY_USER_ID, this.modifyUserIdIn);
        return this;
    }

    public SmsCriteria setOrModifyUserIdIn(String... modifyUserIds) {
        this.orModifyUserIdIn = CollUtil.newHashSet(modifyUserIds);
        orIn(Sms.MODIFY_USER_ID, this.orModifyUserIdIn);
        return this;
    }

    public SmsCriteria setModifyUserIdNotIn(String... modifyUserIds) {
        this.modifyUserIdNotIn = CollUtil.newHashSet(modifyUserIds);
        notIn(Sms.MODIFY_USER_ID, this.modifyUserIdNotIn);
        return this;
    }

    public SmsCriteria setOrModifyUserIdNotIn(String... modifyUserIds) {
        this.orModifyUserIdNotIn = CollUtil.newHashSet(modifyUserIds);
        orNotIn(Sms.MODIFY_USER_ID, this.orModifyUserIdNotIn);
        return this;
    }

    public SmsCriteria setModifyUserIdLike(String modifyUserIdLike) {
        this.modifyUserIdLike = modifyUserIdLike == null ? null : modifyUserIdLike.trim();
        like(Sms.MODIFY_USER_ID, this.modifyUserIdLike);
        return this;
    }

    public SmsCriteria setOrModifyUserIdLike(String orModifyUserIdLike) {
        this.orModifyUserIdLike = orModifyUserIdLike == null ? null : orModifyUserIdLike.trim();
        orLike(Sms.MODIFY_USER_ID, this.orModifyUserIdLike);
        return this;
    }

    public SmsCriteria setModifyUserIdNotLike(String modifyUserIdNotLike) {
        this.modifyUserIdNotLike = modifyUserIdNotLike == null ? null : modifyUserIdNotLike.trim();
        notLike(Sms.MODIFY_USER_ID, this.modifyUserIdNotLike);
        return this;
    }

    public SmsCriteria setOrModifyUserIdNotLike(String orModifyUserIdNotLike) {
        this.orModifyUserIdNotLike = orModifyUserIdNotLike == null ? null : orModifyUserIdNotLike.trim();
        orNotLike(Sms.MODIFY_USER_ID, this.orModifyUserIdNotLike);
        return this;
    }

    public SmsCriteria setModifyUserIdStartWith(String modifyUserIdStartWith) {
        this.modifyUserIdStartWith = modifyUserIdStartWith == null ? null : modifyUserIdStartWith.trim();
        startWith(Sms.MODIFY_USER_ID, modifyUserIdStartWith);
        return this;
    }

    public SmsCriteria setOrModifyUserIdStartWith(String orModifyUserIdStartWith) {
        this.orModifyUserIdStartWith = orModifyUserIdStartWith == null ? null : orModifyUserIdStartWith.trim();
        orStartWith(Sms.MODIFY_USER_ID, orModifyUserIdStartWith);
        return this;
    }

    //************************************************** modifyUserName setter **************************************************//

    public SmsCriteria setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
        equals(Sms.MODIFY_USER_NAME, this.modifyUserName);
        return this;
    }

    public SmsCriteria setOrModifyUserName(String modifyUserName) {
        this.orModifyUserName = modifyUserName;
        orEquals(Sms.MODIFY_USER_NAME, this.orModifyUserName);
        return this;
    }

    public SmsCriteria setModifyUserNameNotEqual(String modifyUserNameNotEqual) {
        this.modifyUserNameNotEqual = modifyUserNameNotEqual;
        notEquals(Sms.MODIFY_USER_NAME, this.modifyUserNameNotEqual);
        return this;
    }

    public SmsCriteria setOrModifyUserNameNotEqual(String orModifyUserNameNotEqual) {
        this.orModifyUserNameNotEqual = orModifyUserNameNotEqual;
        orNotEquals(Sms.MODIFY_USER_NAME, this.orModifyUserNameNotEqual);
        return this;
    }

    public SmsCriteria setModifyUserNameIn(Collection<String> modifyUserNames) {
        this.modifyUserNameIn = modifyUserNames;
        in(Sms.MODIFY_USER_NAME, this.modifyUserNameIn);
        return this;
    }

    public SmsCriteria setOrModifyUserNameIn(Collection<String> modifyUserNames) {
        this.orModifyUserNameIn = modifyUserNames;
        orIn(Sms.MODIFY_USER_NAME, this.orModifyUserNameIn);
        return this;
    }

    public SmsCriteria setModifyUserNameNotIn(Collection<String> modifyUserNames) {
        this.modifyUserNameNotIn = modifyUserNames;
        notIn(Sms.MODIFY_USER_NAME, this.modifyUserNameNotIn);
        return this;
    }

    public SmsCriteria setOrModifyUserNameNotIn(Collection<String> modifyUserNames) {
        this.orModifyUserNameNotIn = modifyUserNames;
        orNotIn(Sms.MODIFY_USER_NAME, this.orModifyUserNameNotIn);
        return this;
    }

    public SmsCriteria setModifyUserNameIn(String... modifyUserNames) {
        this.modifyUserNameIn = CollUtil.newHashSet(modifyUserNames);
        in(Sms.MODIFY_USER_NAME, this.modifyUserNameIn);
        return this;
    }

    public SmsCriteria setOrModifyUserNameIn(String... modifyUserNames) {
        this.orModifyUserNameIn = CollUtil.newHashSet(modifyUserNames);
        orIn(Sms.MODIFY_USER_NAME, this.orModifyUserNameIn);
        return this;
    }

    public SmsCriteria setModifyUserNameNotIn(String... modifyUserNames) {
        this.modifyUserNameNotIn = CollUtil.newHashSet(modifyUserNames);
        notIn(Sms.MODIFY_USER_NAME, this.modifyUserNameNotIn);
        return this;
    }

    public SmsCriteria setOrModifyUserNameNotIn(String... modifyUserNames) {
        this.orModifyUserNameNotIn = CollUtil.newHashSet(modifyUserNames);
        orNotIn(Sms.MODIFY_USER_NAME, this.orModifyUserNameNotIn);
        return this;
    }

    public SmsCriteria setModifyUserNameLike(String modifyUserNameLike) {
        this.modifyUserNameLike = modifyUserNameLike == null ? null : modifyUserNameLike.trim();
        like(Sms.MODIFY_USER_NAME, this.modifyUserNameLike);
        return this;
    }

    public SmsCriteria setOrModifyUserNameLike(String orModifyUserNameLike) {
        this.orModifyUserNameLike = orModifyUserNameLike == null ? null : orModifyUserNameLike.trim();
        orLike(Sms.MODIFY_USER_NAME, this.orModifyUserNameLike);
        return this;
    }

    public SmsCriteria setModifyUserNameNotLike(String modifyUserNameNotLike) {
        this.modifyUserNameNotLike = modifyUserNameNotLike == null ? null : modifyUserNameNotLike.trim();
        notLike(Sms.MODIFY_USER_NAME, this.modifyUserNameNotLike);
        return this;
    }

    public SmsCriteria setOrModifyUserNameNotLike(String orModifyUserNameNotLike) {
        this.orModifyUserNameNotLike = orModifyUserNameNotLike == null ? null : orModifyUserNameNotLike.trim();
        orNotLike(Sms.MODIFY_USER_NAME, this.orModifyUserNameNotLike);
        return this;
    }

    public SmsCriteria setModifyUserNameStartWith(String modifyUserNameStartWith) {
        this.modifyUserNameStartWith = modifyUserNameStartWith == null ? null : modifyUserNameStartWith.trim();
        startWith(Sms.MODIFY_USER_NAME, modifyUserNameStartWith);
        return this;
    }

    public SmsCriteria setOrModifyUserNameStartWith(String orModifyUserNameStartWith) {
        this.orModifyUserNameStartWith = orModifyUserNameStartWith == null ? null : orModifyUserNameStartWith.trim();
        orStartWith(Sms.MODIFY_USER_NAME, orModifyUserNameStartWith);
        return this;
    }

    //************************************************** modifyTime setter **************************************************//

    public SmsCriteria setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
        equals(Sms.MODIFY_TIME, this.modifyTime);
        return this;
    }

    public SmsCriteria setOrModifyTime(LocalDateTime modifyTime) {
        this.orModifyTime = modifyTime;
        orEquals(Sms.MODIFY_TIME, this.orModifyTime);
        return this;
    }

    public SmsCriteria setModifyTimeNotEqual(LocalDateTime modifyTimeNotEqual) {
        this.modifyTimeNotEqual = modifyTimeNotEqual;
        notEquals(Sms.MODIFY_TIME, this.modifyTimeNotEqual);
        return this;
    }

    public SmsCriteria setOrModifyTimeNotEqual(LocalDateTime orModifyTimeNotEqual) {
        this.orModifyTimeNotEqual = orModifyTimeNotEqual;
        orNotEquals(Sms.MODIFY_TIME, this.orModifyTimeNotEqual);
        return this;
    }

    public SmsCriteria setModifyTimeIn(Collection<LocalDateTime> modifyTimes) {
        this.modifyTimeIn = modifyTimes;
        in(Sms.MODIFY_TIME, this.modifyTimeIn);
        return this;
    }

    public SmsCriteria setOrModifyTimeIn(Collection<LocalDateTime> modifyTimes) {
        this.orModifyTimeIn = modifyTimes;
        orIn(Sms.MODIFY_TIME, this.orModifyTimeIn);
        return this;
    }

    public SmsCriteria setModifyTimeNotIn(Collection<LocalDateTime> modifyTimes) {
        this.modifyTimeNotIn = modifyTimes;
        notIn(Sms.MODIFY_TIME, this.modifyTimeNotIn);
        return this;
    }

    public SmsCriteria setOrModifyTimeNotIn(Collection<LocalDateTime> modifyTimes) {
        this.orModifyTimeNotIn = modifyTimes;
        orNotIn(Sms.MODIFY_TIME, this.orModifyTimeNotIn);
        return this;
    }

    public SmsCriteria setModifyTimeIn(LocalDateTime... modifyTimes) {
        this.modifyTimeIn = CollUtil.newHashSet(modifyTimes);
        in(Sms.MODIFY_TIME, this.modifyTimeIn);
        return this;
    }

    public SmsCriteria setOrModifyTimeIn(LocalDateTime... modifyTimes) {
        this.orModifyTimeIn = CollUtil.newHashSet(modifyTimes);
        orIn(Sms.MODIFY_TIME, this.orModifyTimeIn);
        return this;
    }

    public SmsCriteria setModifyTimeNotIn(LocalDateTime... modifyTimes) {
        this.modifyTimeNotIn = CollUtil.newHashSet(modifyTimes);
        notIn(Sms.MODIFY_TIME, this.modifyTimeNotIn);
        return this;
    }

    public SmsCriteria setOrModifyTimeNotIn(LocalDateTime... modifyTimes) {
        this.orModifyTimeNotIn = CollUtil.newHashSet(modifyTimes);
        orNotIn(Sms.MODIFY_TIME, this.orModifyTimeNotIn);
        return this;
    }

    public SmsCriteria setModifyTimeLessThan(LocalDateTime modifyTimeLessThan) {
        this.modifyTimeLessThan = modifyTimeLessThan;
        lessThan(Sms.MODIFY_TIME, this.modifyTimeLessThan);
        return this;
    }

    public SmsCriteria setOrModifyTimeLessThan(LocalDateTime orModifyTimeLessThan) {
        this.orModifyTimeLessThan = orModifyTimeLessThan;
        orLessThan(Sms.MODIFY_TIME, this.orModifyTimeLessThan);
        return this;
    }

    public SmsCriteria setModifyTimeLessThanEqual(LocalDateTime modifyTimeLessThanEqual) {
        this.modifyTimeLessThanEqual = modifyTimeLessThanEqual;
        lessThanEqual(Sms.MODIFY_TIME, this.modifyTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setOrModifyTimeLessThanEqual(LocalDateTime orModifyTimeLessThanEqual) {
        this.orModifyTimeLessThanEqual = orModifyTimeLessThanEqual;
        orLessThanEqual(Sms.MODIFY_TIME, this.orModifyTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setModifyTimeGreaterThan(LocalDateTime modifyTimeGreaterThan) {
        this.modifyTimeGreaterThan = modifyTimeGreaterThan;
        greaterThan(Sms.MODIFY_TIME, this.modifyTimeGreaterThan);
        return this;
    }

    public SmsCriteria setOrModifyTimeGreaterThan(LocalDateTime orModifyTimeGreaterThan) {
        this.orModifyTimeGreaterThan = orModifyTimeGreaterThan;
        orGreaterThan(Sms.MODIFY_TIME, this.orModifyTimeGreaterThan);
        return this;
    }

    public SmsCriteria setModifyTimeGreaterThanEqual(LocalDateTime modifyTimeGreaterThanEqual) {
        this.modifyTimeGreaterThanEqual = modifyTimeGreaterThanEqual;
        greaterThanEqual(Sms.MODIFY_TIME, this.modifyTimeGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrModifyTimeGreaterThanEqual(LocalDateTime orModifyTimeGreaterThanEqual) {
        this.orModifyTimeGreaterThanEqual = orModifyTimeGreaterThanEqual;
        orGreaterThanEqual(Sms.MODIFY_TIME, this.orModifyTimeGreaterThanEqual);
        return this;
    }

    //************************************************** receivingMobiles setter **************************************************//

    public SmsCriteria setReceivingMobiles(Collection<String> receivingMobiles) {
        this.receivingMobiles = receivingMobiles;
        equals(Sms.RECEIVING_MOBILES, this.receivingMobiles);
        return this;
    }

    public SmsCriteria setOrReceivingMobiles(Collection<String> receivingMobiles) {
        this.orReceivingMobiles = receivingMobiles;
        orEquals(Sms.RECEIVING_MOBILES, this.orReceivingMobiles);
        return this;
    }

    public SmsCriteria setReceivingMobilesNotEqual(Collection<String> receivingMobilesNotEqual) {
        this.receivingMobilesNotEqual = receivingMobilesNotEqual;
        notEquals(Sms.RECEIVING_MOBILES, this.receivingMobilesNotEqual);
        return this;
    }

    public SmsCriteria setOrReceivingMobilesNotEqual(Collection<String> orReceivingMobilesNotEqual) {
        this.orReceivingMobilesNotEqual = orReceivingMobilesNotEqual;
        orNotEquals(Sms.RECEIVING_MOBILES, this.orReceivingMobilesNotEqual);
        return this;
    }

    public SmsCriteria setReceivingMobilesIn(Collection<Collection<String>> receivingMobiless) {
        this.receivingMobilesIn = receivingMobiless;
        in(Sms.RECEIVING_MOBILES, this.receivingMobilesIn);
        return this;
    }

    public SmsCriteria setOrReceivingMobilesIn(Collection<Collection<String>> receivingMobiless) {
        this.orReceivingMobilesIn = receivingMobiless;
        orIn(Sms.RECEIVING_MOBILES, this.orReceivingMobilesIn);
        return this;
    }

    public SmsCriteria setReceivingMobilesNotIn(Collection<Collection<String>> receivingMobiless) {
        this.receivingMobilesNotIn = receivingMobiless;
        notIn(Sms.RECEIVING_MOBILES, this.receivingMobilesNotIn);
        return this;
    }

    public SmsCriteria setOrReceivingMobilesNotIn(Collection<Collection<String>> receivingMobiless) {
        this.orReceivingMobilesNotIn = receivingMobiless;
        orNotIn(Sms.RECEIVING_MOBILES, this.orReceivingMobilesNotIn);
        return this;
    }

    public SmsCriteria setReceivingMobilesIn(Collection<String>... receivingMobiless) {
        this.receivingMobilesIn = CollUtil.newHashSet(receivingMobiless);
        in(Sms.RECEIVING_MOBILES, this.receivingMobilesIn);
        return this;
    }

    public SmsCriteria setOrReceivingMobilesIn(Collection<String>... receivingMobiless) {
        this.orReceivingMobilesIn = CollUtil.newHashSet(receivingMobiless);
        orIn(Sms.RECEIVING_MOBILES, this.orReceivingMobilesIn);
        return this;
    }

    public SmsCriteria setReceivingMobilesNotIn(Collection<String>... receivingMobiless) {
        this.receivingMobilesNotIn = CollUtil.newHashSet(receivingMobiless);
        notIn(Sms.RECEIVING_MOBILES, this.receivingMobilesNotIn);
        return this;
    }

    public SmsCriteria setOrReceivingMobilesNotIn(Collection<String>... receivingMobiless) {
        this.orReceivingMobilesNotIn = CollUtil.newHashSet(receivingMobiless);
        orNotIn(Sms.RECEIVING_MOBILES, this.orReceivingMobilesNotIn);
        return this;
    }

    public SmsCriteria setReceivingMobilesContains(Collection<String> receivingMobilesContains) {
        this.receivingMobilesContains = receivingMobilesContains;
        collectionContainsAll(Sms.RECEIVING_MOBILES, this.receivingMobilesContains);
        return this;
    }

    //************************************************** sendTime setter **************************************************//

    public SmsCriteria setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
        equals(Sms.SEND_TIME, this.sendTime);
        return this;
    }

    public SmsCriteria setOrSendTime(LocalDateTime sendTime) {
        this.orSendTime = sendTime;
        orEquals(Sms.SEND_TIME, this.orSendTime);
        return this;
    }

    public SmsCriteria setSendTimeNotEqual(LocalDateTime sendTimeNotEqual) {
        this.sendTimeNotEqual = sendTimeNotEqual;
        notEquals(Sms.SEND_TIME, this.sendTimeNotEqual);
        return this;
    }

    public SmsCriteria setOrSendTimeNotEqual(LocalDateTime orSendTimeNotEqual) {
        this.orSendTimeNotEqual = orSendTimeNotEqual;
        orNotEquals(Sms.SEND_TIME, this.orSendTimeNotEqual);
        return this;
    }

    public SmsCriteria setSendTimeIn(Collection<LocalDateTime> sendTimes) {
        this.sendTimeIn = sendTimes;
        in(Sms.SEND_TIME, this.sendTimeIn);
        return this;
    }

    public SmsCriteria setOrSendTimeIn(Collection<LocalDateTime> sendTimes) {
        this.orSendTimeIn = sendTimes;
        orIn(Sms.SEND_TIME, this.orSendTimeIn);
        return this;
    }

    public SmsCriteria setSendTimeNotIn(Collection<LocalDateTime> sendTimes) {
        this.sendTimeNotIn = sendTimes;
        notIn(Sms.SEND_TIME, this.sendTimeNotIn);
        return this;
    }

    public SmsCriteria setOrSendTimeNotIn(Collection<LocalDateTime> sendTimes) {
        this.orSendTimeNotIn = sendTimes;
        orNotIn(Sms.SEND_TIME, this.orSendTimeNotIn);
        return this;
    }

    public SmsCriteria setSendTimeIn(LocalDateTime... sendTimes) {
        this.sendTimeIn = CollUtil.newHashSet(sendTimes);
        in(Sms.SEND_TIME, this.sendTimeIn);
        return this;
    }

    public SmsCriteria setOrSendTimeIn(LocalDateTime... sendTimes) {
        this.orSendTimeIn = CollUtil.newHashSet(sendTimes);
        orIn(Sms.SEND_TIME, this.orSendTimeIn);
        return this;
    }

    public SmsCriteria setSendTimeNotIn(LocalDateTime... sendTimes) {
        this.sendTimeNotIn = CollUtil.newHashSet(sendTimes);
        notIn(Sms.SEND_TIME, this.sendTimeNotIn);
        return this;
    }

    public SmsCriteria setOrSendTimeNotIn(LocalDateTime... sendTimes) {
        this.orSendTimeNotIn = CollUtil.newHashSet(sendTimes);
        orNotIn(Sms.SEND_TIME, this.orSendTimeNotIn);
        return this;
    }

    public SmsCriteria setSendTimeLessThan(LocalDateTime sendTimeLessThan) {
        this.sendTimeLessThan = sendTimeLessThan;
        lessThan(Sms.SEND_TIME, this.sendTimeLessThan);
        return this;
    }

    public SmsCriteria setOrSendTimeLessThan(LocalDateTime orSendTimeLessThan) {
        this.orSendTimeLessThan = orSendTimeLessThan;
        orLessThan(Sms.SEND_TIME, this.orSendTimeLessThan);
        return this;
    }

    public SmsCriteria setSendTimeLessThanEqual(LocalDateTime sendTimeLessThanEqual) {
        this.sendTimeLessThanEqual = sendTimeLessThanEqual;
        lessThanEqual(Sms.SEND_TIME, this.sendTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setOrSendTimeLessThanEqual(LocalDateTime orSendTimeLessThanEqual) {
        this.orSendTimeLessThanEqual = orSendTimeLessThanEqual;
        orLessThanEqual(Sms.SEND_TIME, this.orSendTimeLessThanEqual);
        return this;
    }

    public SmsCriteria setSendTimeGreaterThan(LocalDateTime sendTimeGreaterThan) {
        this.sendTimeGreaterThan = sendTimeGreaterThan;
        greaterThan(Sms.SEND_TIME, this.sendTimeGreaterThan);
        return this;
    }

    public SmsCriteria setOrSendTimeGreaterThan(LocalDateTime orSendTimeGreaterThan) {
        this.orSendTimeGreaterThan = orSendTimeGreaterThan;
        orGreaterThan(Sms.SEND_TIME, this.orSendTimeGreaterThan);
        return this;
    }

    public SmsCriteria setSendTimeGreaterThanEqual(LocalDateTime sendTimeGreaterThanEqual) {
        this.sendTimeGreaterThanEqual = sendTimeGreaterThanEqual;
        greaterThanEqual(Sms.SEND_TIME, this.sendTimeGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrSendTimeGreaterThanEqual(LocalDateTime orSendTimeGreaterThanEqual) {
        this.orSendTimeGreaterThanEqual = orSendTimeGreaterThanEqual;
        orGreaterThanEqual(Sms.SEND_TIME, this.orSendTimeGreaterThanEqual);
        return this;
    }

    //************************************************** content setter **************************************************//

    public SmsCriteria setContent(String content) {
        this.content = content;
        equals(Sms.CONTENT, this.content);
        return this;
    }

    public SmsCriteria setOrContent(String content) {
        this.orContent = content;
        orEquals(Sms.CONTENT, this.orContent);
        return this;
    }

    public SmsCriteria setContentNotEqual(String contentNotEqual) {
        this.contentNotEqual = contentNotEqual;
        notEquals(Sms.CONTENT, this.contentNotEqual);
        return this;
    }

    public SmsCriteria setOrContentNotEqual(String orContentNotEqual) {
        this.orContentNotEqual = orContentNotEqual;
        orNotEquals(Sms.CONTENT, this.orContentNotEqual);
        return this;
    }

    public SmsCriteria setContentIn(Collection<String> contents) {
        this.contentIn = contents;
        in(Sms.CONTENT, this.contentIn);
        return this;
    }

    public SmsCriteria setOrContentIn(Collection<String> contents) {
        this.orContentIn = contents;
        orIn(Sms.CONTENT, this.orContentIn);
        return this;
    }

    public SmsCriteria setContentNotIn(Collection<String> contents) {
        this.contentNotIn = contents;
        notIn(Sms.CONTENT, this.contentNotIn);
        return this;
    }

    public SmsCriteria setOrContentNotIn(Collection<String> contents) {
        this.orContentNotIn = contents;
        orNotIn(Sms.CONTENT, this.orContentNotIn);
        return this;
    }

    public SmsCriteria setContentIn(String... contents) {
        this.contentIn = CollUtil.newHashSet(contents);
        in(Sms.CONTENT, this.contentIn);
        return this;
    }

    public SmsCriteria setOrContentIn(String... contents) {
        this.orContentIn = CollUtil.newHashSet(contents);
        orIn(Sms.CONTENT, this.orContentIn);
        return this;
    }

    public SmsCriteria setContentNotIn(String... contents) {
        this.contentNotIn = CollUtil.newHashSet(contents);
        notIn(Sms.CONTENT, this.contentNotIn);
        return this;
    }

    public SmsCriteria setOrContentNotIn(String... contents) {
        this.orContentNotIn = CollUtil.newHashSet(contents);
        orNotIn(Sms.CONTENT, this.orContentNotIn);
        return this;
    }

    public SmsCriteria setContentLike(String contentLike) {
        this.contentLike = contentLike == null ? null : contentLike.trim();
        like(Sms.CONTENT, this.contentLike);
        return this;
    }

    public SmsCriteria setOrContentLike(String orContentLike) {
        this.orContentLike = orContentLike == null ? null : orContentLike.trim();
        orLike(Sms.CONTENT, this.orContentLike);
        return this;
    }

    public SmsCriteria setContentNotLike(String contentNotLike) {
        this.contentNotLike = contentNotLike == null ? null : contentNotLike.trim();
        notLike(Sms.CONTENT, this.contentNotLike);
        return this;
    }

    public SmsCriteria setOrContentNotLike(String orContentNotLike) {
        this.orContentNotLike = orContentNotLike == null ? null : orContentNotLike.trim();
        orNotLike(Sms.CONTENT, this.orContentNotLike);
        return this;
    }

    public SmsCriteria setContentStartWith(String contentStartWith) {
        this.contentStartWith = contentStartWith == null ? null : contentStartWith.trim();
        startWith(Sms.CONTENT, contentStartWith);
        return this;
    }

    public SmsCriteria setOrContentStartWith(String orContentStartWith) {
        this.orContentStartWith = orContentStartWith == null ? null : orContentStartWith.trim();
        orStartWith(Sms.CONTENT, orContentStartWith);
        return this;
    }

    //************************************************** bizType setter **************************************************//

    public SmsCriteria setBizType(Integer bizType) {
        this.bizType = bizType;
        equals(Sms.BIZ_TYPE, this.bizType);
        return this;
    }

    public SmsCriteria setOrBizType(Integer bizType) {
        this.orBizType = bizType;
        orEquals(Sms.BIZ_TYPE, this.orBizType);
        return this;
    }

    public SmsCriteria setBizTypeNotEqual(Integer bizTypeNotEqual) {
        this.bizTypeNotEqual = bizTypeNotEqual;
        notEquals(Sms.BIZ_TYPE, this.bizTypeNotEqual);
        return this;
    }

    public SmsCriteria setOrBizTypeNotEqual(Integer orBizTypeNotEqual) {
        this.orBizTypeNotEqual = orBizTypeNotEqual;
        orNotEquals(Sms.BIZ_TYPE, this.orBizTypeNotEqual);
        return this;
    }

    public SmsCriteria setBizTypeIn(Collection<Integer> bizTypes) {
        this.bizTypeIn = bizTypes;
        in(Sms.BIZ_TYPE, this.bizTypeIn);
        return this;
    }

    public SmsCriteria setOrBizTypeIn(Collection<Integer> bizTypes) {
        this.orBizTypeIn = bizTypes;
        orIn(Sms.BIZ_TYPE, this.orBizTypeIn);
        return this;
    }

    public SmsCriteria setBizTypeNotIn(Collection<Integer> bizTypes) {
        this.bizTypeNotIn = bizTypes;
        notIn(Sms.BIZ_TYPE, this.bizTypeNotIn);
        return this;
    }

    public SmsCriteria setOrBizTypeNotIn(Collection<Integer> bizTypes) {
        this.orBizTypeNotIn = bizTypes;
        orNotIn(Sms.BIZ_TYPE, this.orBizTypeNotIn);
        return this;
    }

    public SmsCriteria setBizTypeIn(Integer... bizTypes) {
        this.bizTypeIn = CollUtil.newHashSet(bizTypes);
        in(Sms.BIZ_TYPE, this.bizTypeIn);
        return this;
    }

    public SmsCriteria setOrBizTypeIn(Integer... bizTypes) {
        this.orBizTypeIn = CollUtil.newHashSet(bizTypes);
        orIn(Sms.BIZ_TYPE, this.orBizTypeIn);
        return this;
    }

    public SmsCriteria setBizTypeNotIn(Integer... bizTypes) {
        this.bizTypeNotIn = CollUtil.newHashSet(bizTypes);
        notIn(Sms.BIZ_TYPE, this.bizTypeNotIn);
        return this;
    }

    public SmsCriteria setOrBizTypeNotIn(Integer... bizTypes) {
        this.orBizTypeNotIn = CollUtil.newHashSet(bizTypes);
        orNotIn(Sms.BIZ_TYPE, this.orBizTypeNotIn);
        return this;
    }

    public SmsCriteria setBizTypeLessThan(Integer bizTypeLessThan) {
        this.bizTypeLessThan = bizTypeLessThan;
        lessThan(Sms.BIZ_TYPE, this.bizTypeLessThan);
        return this;
    }

    public SmsCriteria setOrBizTypeLessThan(Integer orBizTypeLessThan) {
        this.orBizTypeLessThan = orBizTypeLessThan;
        orLessThan(Sms.BIZ_TYPE, this.orBizTypeLessThan);
        return this;
    }

    public SmsCriteria setBizTypeLessThanEqual(Integer bizTypeLessThanEqual) {
        this.bizTypeLessThanEqual = bizTypeLessThanEqual;
        lessThanEqual(Sms.BIZ_TYPE, this.bizTypeLessThanEqual);
        return this;
    }

    public SmsCriteria setOrBizTypeLessThanEqual(Integer orBizTypeLessThanEqual) {
        this.orBizTypeLessThanEqual = orBizTypeLessThanEqual;
        orLessThanEqual(Sms.BIZ_TYPE, this.orBizTypeLessThanEqual);
        return this;
    }

    public SmsCriteria setBizTypeGreaterThan(Integer bizTypeGreaterThan) {
        this.bizTypeGreaterThan = bizTypeGreaterThan;
        greaterThan(Sms.BIZ_TYPE, this.bizTypeGreaterThan);
        return this;
    }

    public SmsCriteria setOrBizTypeGreaterThan(Integer orBizTypeGreaterThan) {
        this.orBizTypeGreaterThan = orBizTypeGreaterThan;
        orGreaterThan(Sms.BIZ_TYPE, this.orBizTypeGreaterThan);
        return this;
    }

    public SmsCriteria setBizTypeGreaterThanEqual(Integer bizTypeGreaterThanEqual) {
        this.bizTypeGreaterThanEqual = bizTypeGreaterThanEqual;
        greaterThanEqual(Sms.BIZ_TYPE, this.bizTypeGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrBizTypeGreaterThanEqual(Integer orBizTypeGreaterThanEqual) {
        this.orBizTypeGreaterThanEqual = orBizTypeGreaterThanEqual;
        orGreaterThanEqual(Sms.BIZ_TYPE, this.orBizTypeGreaterThanEqual);
        return this;
    }

    //************************************************** sendCount setter **************************************************//

    public SmsCriteria setSendCount(Integer sendCount) {
        this.sendCount = sendCount;
        equals(Sms.SEND_COUNT, this.sendCount);
        return this;
    }

    public SmsCriteria setOrSendCount(Integer sendCount) {
        this.orSendCount = sendCount;
        orEquals(Sms.SEND_COUNT, this.orSendCount);
        return this;
    }

    public SmsCriteria setSendCountNotEqual(Integer sendCountNotEqual) {
        this.sendCountNotEqual = sendCountNotEqual;
        notEquals(Sms.SEND_COUNT, this.sendCountNotEqual);
        return this;
    }

    public SmsCriteria setOrSendCountNotEqual(Integer orSendCountNotEqual) {
        this.orSendCountNotEqual = orSendCountNotEqual;
        orNotEquals(Sms.SEND_COUNT, this.orSendCountNotEqual);
        return this;
    }

    public SmsCriteria setSendCountIn(Collection<Integer> sendCounts) {
        this.sendCountIn = sendCounts;
        in(Sms.SEND_COUNT, this.sendCountIn);
        return this;
    }

    public SmsCriteria setOrSendCountIn(Collection<Integer> sendCounts) {
        this.orSendCountIn = sendCounts;
        orIn(Sms.SEND_COUNT, this.orSendCountIn);
        return this;
    }

    public SmsCriteria setSendCountNotIn(Collection<Integer> sendCounts) {
        this.sendCountNotIn = sendCounts;
        notIn(Sms.SEND_COUNT, this.sendCountNotIn);
        return this;
    }

    public SmsCriteria setOrSendCountNotIn(Collection<Integer> sendCounts) {
        this.orSendCountNotIn = sendCounts;
        orNotIn(Sms.SEND_COUNT, this.orSendCountNotIn);
        return this;
    }

    public SmsCriteria setSendCountIn(Integer... sendCounts) {
        this.sendCountIn = CollUtil.newHashSet(sendCounts);
        in(Sms.SEND_COUNT, this.sendCountIn);
        return this;
    }

    public SmsCriteria setOrSendCountIn(Integer... sendCounts) {
        this.orSendCountIn = CollUtil.newHashSet(sendCounts);
        orIn(Sms.SEND_COUNT, this.orSendCountIn);
        return this;
    }

    public SmsCriteria setSendCountNotIn(Integer... sendCounts) {
        this.sendCountNotIn = CollUtil.newHashSet(sendCounts);
        notIn(Sms.SEND_COUNT, this.sendCountNotIn);
        return this;
    }

    public SmsCriteria setOrSendCountNotIn(Integer... sendCounts) {
        this.orSendCountNotIn = CollUtil.newHashSet(sendCounts);
        orNotIn(Sms.SEND_COUNT, this.orSendCountNotIn);
        return this;
    }

    public SmsCriteria setSendCountLessThan(Integer sendCountLessThan) {
        this.sendCountLessThan = sendCountLessThan;
        lessThan(Sms.SEND_COUNT, this.sendCountLessThan);
        return this;
    }

    public SmsCriteria setOrSendCountLessThan(Integer orSendCountLessThan) {
        this.orSendCountLessThan = orSendCountLessThan;
        orLessThan(Sms.SEND_COUNT, this.orSendCountLessThan);
        return this;
    }

    public SmsCriteria setSendCountLessThanEqual(Integer sendCountLessThanEqual) {
        this.sendCountLessThanEqual = sendCountLessThanEqual;
        lessThanEqual(Sms.SEND_COUNT, this.sendCountLessThanEqual);
        return this;
    }

    public SmsCriteria setOrSendCountLessThanEqual(Integer orSendCountLessThanEqual) {
        this.orSendCountLessThanEqual = orSendCountLessThanEqual;
        orLessThanEqual(Sms.SEND_COUNT, this.orSendCountLessThanEqual);
        return this;
    }

    public SmsCriteria setSendCountGreaterThan(Integer sendCountGreaterThan) {
        this.sendCountGreaterThan = sendCountGreaterThan;
        greaterThan(Sms.SEND_COUNT, this.sendCountGreaterThan);
        return this;
    }

    public SmsCriteria setOrSendCountGreaterThan(Integer orSendCountGreaterThan) {
        this.orSendCountGreaterThan = orSendCountGreaterThan;
        orGreaterThan(Sms.SEND_COUNT, this.orSendCountGreaterThan);
        return this;
    }

    public SmsCriteria setSendCountGreaterThanEqual(Integer sendCountGreaterThanEqual) {
        this.sendCountGreaterThanEqual = sendCountGreaterThanEqual;
        greaterThanEqual(Sms.SEND_COUNT, this.sendCountGreaterThanEqual);
        return this;
    }

    public SmsCriteria setOrSendCountGreaterThanEqual(Integer orSendCountGreaterThanEqual) {
        this.orSendCountGreaterThanEqual = orSendCountGreaterThanEqual;
        orGreaterThanEqual(Sms.SEND_COUNT, this.orSendCountGreaterThanEqual);
        return this;
    }

}

```