# 快速开始

我们将通过创建一个简单的 demo 项目来，并设计一张用户表，来阐述 isass 的强大功能。

## 1.下载项目

isass 提供微服务项目初始化（脚手架）下载接口。开发者可以打开 api 文档调用基础微服务的接口，下载新项目代码。

::: tip

通过接口下载新项目，需要用到 `基础微服务(base-service)`

:::

接口文档地址：
> http://ip:port/base-service/doc.html#/default/项目代码初始化/getGenerateCodeZipUsingGET

- 打开 api 聚合文档，阅读请求参数后，切换到 `调试` tab，输入参数，点击发送，即可下载微服务项目代码

- 如果没有自动弹出下载窗口，则切换到 `Curl` tab，复制出请求连接，放到浏览器地址栏访问即可下载项目代码

<img :src="$withBase('/img/project-initializr.jpg')" alt="文档地址">

<img :src="$withBase('/img/project-initializr-curl.jpg')" alt="复制curl到地址栏">


## 2.导入项目

将下载回来的压缩包解压，项目代码导入到 IDE，以下以 IntelliJ IDEA 为例

* File -> New -> project from existing source -> 选中项目代码根目录的 pom.xml文件 -> OK -> 等待 idea 下方进度栏导入完成

<img :src="$withBase('/img/project-from-existing-source.jpg')" alt="project-from-existing-source">

<img :src="$withBase('/img/demo-pom.jpg')" alt="demo-pom">

* 导入成功后的效果图如下

<img :src="$withBase('/img/open-project.jpg')" alt="open-project">

- 开启 lombok 

项目导入后，右下角有可能会弹出开启 `lombok` 的窗口，请选择 `Enable`

<img :src="$withBase('/img/lombok.jpg')" alt="lombok">

* 配置项目的编译器为 jdk8

File -> settings
<img :src="$withBase('/img/java-compiler.png')" alt="java-compiler">

File -> project-structure

<img :src="$withBase('/img/project-structure.jpg')" alt="project-structure">
<img :src="$withBase('/img/project-structure-jdk.jpg')" alt="project-structure-jdk">

- 修改 idea 的 properties 文件编码

File -> settings -> File Encodings

<img :src="$withBase('/img/properties-encoding.jpg')" alt="properties-encoding">

- 修改数据库和 redis 配置

::: tip

正式的项目开发情况下请使用 nacos 进行配置

:::

打开 application.properties 文件，找到对应配置项进行修改

<img :src="$withBase('/img/properties-config.jpg')" alt="properties-config">

## 3.运行项目

点击右上角调试启动按钮，启动成功后，会打印 `Started DemoApp in xx seconds` 信息

<img :src="$withBase('/img/run-project.jpg')" alt="run-project">

## 4.打开 api 文档

- isass 使用优秀的开源项目 knife4j ([https://gitee.com/xiaoym/knife4j](https://gitee.com/xiaoym/knife4j)) 作为接口文档，knife4j 是为 Java 框架集成 Swagger 生成 Api 文档的增强解决方案。

- 方式1：在浏览器中打开接口文档连接：[http://ip:port/demo-service/doc.html](http://ip:port/demo-service/doc.html)

- 方式2：使用 idea 快速打开
  
  选择 Acutator -> Mappings -> 键盘输入 `doc` 快速定位到 `/demo-service/doc.html` -> 右键 -> Mark as Default Path -> 以后就可以点击图片 setp2 的浏览器图标直接打开 api 文档

<img :src="$withBase('/img/doc-mapping.jpg')" alt="doc-mapping">

<img :src="$withBase('/img/make-doc-default.jpg')" alt="make-doc-default">

<img :src="$withBase('/img/apidoc.jpg')" alt="apidoc">

## 5.新建表

设计一张用户 `demo_user` 表，其表结构和数据如下：

| id | nick_name | version | delete_flag | create_user_id | create_user_name | create_time         | modify_user_id | modify_user_name | modify_time         |
|----|-----------|---------|-------------|----------------|------------------|---------------------|----------------|------------------|---------------------|
| 1  | tom       | 1       | 0           | 1              | tom              | 2020-01-01 12:12:00 | 1              | tom              | 2020-01-01 12:12:00 |
| 2  | jack      | 1       | 0           | 2              | jack             | 2020-01-01 12:12:00 | 2              | jack             | 2020-01-01 12:12:00 |

其对应的 mysql 数据库脚本如下：

``` sql
CREATE TABLE demo_user (
    id               BIGINT(20)   NOT NULL              COMMENT '主键',
    nick_name        VARCHAR(32)  NOT NULL DEFAULT ''   COMMENT '昵称',
    version          INT(11)      NOT NULL DEFAULT '0'  COMMENT '版本号',
    delete_flag      TINYINT(1)   NOT NULL DEFAULT '0'  COMMENT '删除标记',
    create_user_id   VARCHAR(64)  NOT NULL DEFAULT ''   COMMENT '创建用户的 id',
    create_user_name VARCHAR(64)  NOT NULL DEFAULT ''   COMMENT '创建用户的名称',
    create_time      DATETIME              DEFAULT NULL COMMENT '创建时间',
    modify_user_id   VARCHAR(64)  NOT NULL DEFAULT ''   COMMENT '修改用户的 id',
    modify_user_name VARCHAR(64)  NOT NULL DEFAULT ''   COMMENT '修改用户的名称',
    modify_time      DATETIME              DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4 
    COMMENT ='用户';
```

其对应数据脚本如下：

``` sql
INSERT INTO demo_user (id, nick_name, version, delete_flag, create_user_id, create_user_name, create_time, modify_user_id, modify_user_name, modify_time) VALUES (1, 'tom', 1, 0, '1', 'tom', '2020-01-01 12:12:00', '1', 'tom', '2020-01-01 12:12:00');
INSERT INTO demo_user (id, nick_name, version, delete_flag, create_user_id, create_user_name, create_time, modify_user_id, modify_user_name, modify_time) VALUES (2, 'jack', 1, 0, '2', 'jack', '2020-01-01 12:12:00', '2', 'jack', '2020-01-01 12:12:00');
```

- 请在数据库中执行以上脚本

## 6.代码生成

根据 demo_user 表，生成代码。每张数据库表将会成10个源码文件

* 打开 DemoMybatisPlusGenerator 类，该类位于 {com}-service-demo-service -> src -> test

- 修改数据库连接参数和表名

* 运行 main 方法，生成代码

<img :src="$withBase('/img/generator.jpg')" alt="generator">

### 6.1.复制生成代码到项目

复制生成的代码到对应的项目模块

<img :src="$withBase('/img/copy-code.png')" alt="copy-code">

### 6.2.重启项目

重启项目后，刷新 api 文档，即可看见用户表的通用接口

<img :src="$withBase('/img/restart-project.jpg')" alt="restart-project">

<img :src="$withBase('/img/demo-user-apidoc.jpg')" alt="demo-user-apidoc">

可对接口调试，测试是否可用，数据是否正确返回

<img :src="$withBase('/img/test-demo-user-api.jpg')" alt="test-demo-user-api">

## 7.小结

通过以上步骤，我们实现了 demo_user 表的 CRUD 功能以及接口文档和接口调试！

从以上步骤中，我们可以看到集成 `isass` 非常的简单，只需要设计好所需的表，生成代码，放置到对应目录即可。
