# 字典功能

`基础微服务` 提供 `字典` 模块，供业务微服务使用。

::: tip 约定
- 字典是树结构，约定使用微服务名称作为顶级目录，方便维护
- 字典编码建议使用英文加下划线命名
:::

## 使用
### 1、字典初始化

有项目使用时再优化初始化功能并完善文档

### 2、字典维护

字典增删改查

<img :src="$withBase('/img/dict.png')"/>

### 3、前端应用
#### 1、获取字典项列表
获取 `性别` 的字典（编码为`SEX`），发起以下 get 请求即可
> /base-service/dict/dictCode/SEX/subDicts

响应内容如下（字典项已按照`排序`字段升序）
``` json 
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": [
    {
      "id": "2",
      "parentId": "1",
      "dictName": "男",
      "dictCode": "MAN"
    },
    {
      "id": "3",
      "parentId": "1",
      "dictName": "女",
      "dictCode": "WOMAN"
    },
    {
      "id": "4",
      "parentId": "1",
      "dictName": "未知",
      "dictCode": "UNKNOW"
    }
  ]
}
```

#### 2、获取单个字典项名称
获取 `性别(SEX)` 的字典项下，编码为 `MAN` 的名称，发起以下 get 请求即可
> /base-service/dict/parentDictCode/SEX/dictCode/MAN/name

响应内容如下
``` json 
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": "男"
}
```

## 表设计

- 字典模块只使用一个表来实现，表名为 `base_dict`

建表 sql 如下：
``` sql
CREATE TABLE base_dict
(
    id                bigint(20)   NOT NULL COMMENT '主键',
    parent_id         bigint(20)   NOT NULL DEFAULT '0' COMMENT '父主键',
    dict_name         varchar(32)  NOT NULL DEFAULT '' COMMENT '名称',
    dict_code         varchar(32)  NOT NULL DEFAULT '' COMMENT '编码',
    order_num         int(11)      NOT NULL DEFAULT '1' COMMENT '排序',
    remark            varchar(256) NOT NULL DEFAULT '' COMMENT '备注',
    new_sub_dict_flag tinyint(1)   NOT NULL DEFAULT '1' COMMENT '能否添加子项',
    editable_flag     tinyint(1)   NOT NULL DEFAULT '1' COMMENT '能否编辑本项',
    deletable_flag    tinyint(1)   NOT NULL DEFAULT '1' COMMENT '能否删除本项',
    create_user_id    varchar(64)  NOT NULL DEFAULT '' COMMENT '创建用户的 id',
    create_user_name  varchar(64)  NOT NULL DEFAULT '' COMMENT '创建用户的用户名',
    create_time       timestamp    NULL     DEFAULT NULL COMMENT '创建时间',
    modify_user_id    varchar(64)  NOT NULL DEFAULT '' COMMENT '修改用户的 id',
    modify_user_name  varchar(64)  NOT NULL DEFAULT '' COMMENT '修改用户的用户名',
    modify_time       timestamp    NULL     DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (id)
)
```
