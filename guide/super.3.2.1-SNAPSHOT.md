# super.3.2.1-SNAPSHOT

## 1 统一部署包与启动脚本

### 1.1 部署包
    
- 构建后生成压缩包：{company}-service-{ms}-service-bin.zip，无论是 jar 包启动，或者 docker 容器启动，都统一使用此部署包。

### 1.2 启动脚本

- 部署包内包含启动脚本 `run.sh`, 直接 jar 包启动，或者 docker 容器启动，都统一调用此脚本进行启动。

### 1.3 启动参数与环境变量

> 在 jar 包直接运行时，改变 `run.sh` 的变量值, 或在 docker 环境运行时设置环境变量，可实现不同功能

#### 1.3.1 变量列表


| 变量 | 默认值 | 说明 |
| --- | ------ | ---- |
| JVM_VARS            | -server -XX:SurvivorRatio=8 -XX:InitialSurvivorRatio=8 -XX:+PrintCommandLineFlags | JVM非内存参数                                                                                          |
| JVM_MEMORY_VARS     | -Xms6G -Xmx6G -Xmn3G -XX:MetaspaceSize=256M -XX:MaxMetaspaceSize=256M             | JVM内存参数                                                                                            |
| JVM_PRINT_GC        | false                                                                             | 是否打印gc信息                                                                                         |
| DEBUG_PORT          |                                                                                   | java 远程调试端口。当设置此值时，本java应用会开启此端口，提供给开发人员进行连接，从而实现代码级别调试  |
| AUTO_TAIL_LOG       | true                                                                              | 启动后是否自动使用 tail 命令打印日志                                                                   |
| RUN_AS_NOHUP        | true                                                                              | 启动 java 的命令是否结合 nohup 进行不挂断运行                                                          |
| KEEP_DOCKER_RUNNING | false                                                                             | 当在 docker 环境中，启动 java 报错后，会导致容器退出，可配置此参数，阻止容器退出，便于进入容器调试问题 |
| WRITE_LOG_TO_FILE   | true                                                                              | 是否使用slf4j把程序日志写到日志文件                                                                    |

#### 1.3.2 容器环境变量默认值差异列表

> 只列出与jar包直接运行时默认值 `不同` 的变量

| 变量              | 默认值 | 差异说明                                              |
| ----------------- | ------ | ----------------------------------------------------- |
| JVM_MEMORY_VARS   |        | 容器环境应该使用 cgroup 限制内存，不通过 jvm 参数限制 |
| AUTO_TAIL_LOG     | false  | 容器环境应该使用 docker logs 查看日志                 |
| RUN_AS_NOHUP      | false  | 容器环境的 java 程序无需后台运行                      |
| WRITE_LOG_TO_FILE | false  | 容器通过控制台收集日志，无需写日志文件                |


## 2 统一配置文件

- 删除原有 `application.properties`，避免生产环境的配置文件与开发环境不一致造成不必要的麻烦。

- service 目录 `resources/config` 新增 `application.yml` `bootstrap.yml`

### 2.1 配置文件说明

- 根据 springCloud 和 nacos 的文档和规范要求，application.yml 是应用程序配置; bootstrap.yml 是启动前预备的配置。例如 nacos 的配置中心必需放在 bootstrap.yml 才生效；应用程序配置，放在 bootstrap.yml 则不会生效，所以他们无法合并成1个配置文件。 一般来说，若官方无特别说明，均放在 application.yml。                      

- resources/application.yml 和 resources/bootstrap.yml                 
 配置基本不需要根据环境作出改变，打包时 `不会外置到部署包`，部署人员不可见，不干扰部署人员。 
                                                                         
- resources/config/application.yml 和 resources/config/bootstrap.yml     
  由部署人员根据环境作出改变，打包时会 `外置到部署包`，部署人员可见。                    
                                                                          
- 若需要在配置文件新增配置时，开发人员结合以上说明，选择合适的配置文件存放配置。       

## 3 新增接口 IMapperLocationProvider

- 解决不同 package 的微服务打包到一个单体时，mapper-locations 配置被覆盖的问题

- 新增接口用于配置 mybatis 的 MapperLocations，各微服务实现此接口，实现类统一放在各微服务的 `service/xxx/db/config` 包下，名称为 `{Ms}MapperLocationProvider`，微服务无需再通过配置项  `mybatis-plus.mapper-locations` 进行配置

- 项目下载模板新增 IMapperLocationProvider 实现类

## 4 新增分布式锁 lock4j 版本依赖管理

- lock4j 是一个分布式锁组件，其提供了多种不同的支持以满足不同性能和环境的需求。
- 支持 `redission` `redisTemplate` `zookeeper` 可混用，支持扩展。
- lock4j 是 mybatis-plus 的作者出品。
- lock4j 项目网址 [gitee](https://gitee.com/baomidou/lock4j)

### 4.1 使用方式

- 业务微服务在 pom 文件添加依赖，版本统一由 core 管理，使用方式参考 [lock4j官网文档](https://gitee.com/baomidou/lock4j)

``` xml
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>lock4j-redisson-spring-boot-starter</artifactId>
</dependency>
```
