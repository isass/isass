# 业务参数配置

`基础微服务` 提供 `业务参数配置` 模块，基本上覆盖所有业务配置的需求，业务微服务无需单独增加参数配置功能。

## 概要设计

- 业务参数配置在参数值的存储上采用 `数组` 形式的 `codeValue`,以覆盖大部分需求的参数配置
- 调用方可根据业务需要，调用不同的接口，实现获取key,value，或者key, code, value 形式的参数


### 储存层面数据格式
``` json
{
    "key": "course",
    "codeValues": [
        {
            "code": "语文",
            "value": "CHINESE"
        },
        {
            "code": "数学",
            "value": "MATHEMATICS"
        }
    ]
}

```

## 获取 string 类型的参数值

### 接口地址 ###

GET /base-service/param/key/{key}/string

### 请求参数 ###


| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 


### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": "abc"
}
```

## 获取 boolean 类型的参数值

### 接口地址 ###

GET /base-service/param/key/{key}/boolean

### 请求参数 ###

| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 

### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": true
}
```

## 获取 int 类型的参数值

### 接口地址 ###

GET /base-service/param/key/{key}/int

### 请求参数 ###

| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 

### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": 123
}
```

## 获取 float 类型的参数值

### 接口地址 ###

GET /base-service/param/key/{key}/float

### 请求参数 ###

| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 

### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": 123.12
}
```

## 获取 单个 codeValue 值

### 接口地址 ###

GET /base-service/param/key/{key}/single

### 请求参数 ###

| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 

### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data":  {
    "code": "age",
    "value": 18
  }
}
```

## 获取 codeValue 列表

### 接口地址 ###

GET /base-service/param/key/{key}

### 请求参数 ###

| 参数名称 | 参数说明  | 参数类型 | 是否必须 | 数据类型  |
| -------- | -------- | -----   | -------- | -------- | 
|key       | key      | path    | true     | string   | 

### 响应示例 ###

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": [
    {
      "code": "语文",
      "value": "CHINESE"
    },
    {
      "code": "数学",
      "value": "MATHEMATICS"
    }
  ]
}
```
