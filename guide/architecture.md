# 架构设计

## 软件架构

<!-- <img :src="$withBase('/img/architecture.jpg')" alt="architecture"> -->
<iframe id="embed_dom" name="embed_dom" frameborder="0" style="display:block;width:680px; height:300px;" src="https://www.processon.com/embed/5ea14bab5653bb6efc667436"></iframe>

### 代码层次

主要分为3个层次：isass-core, isass-api, 微服务项目，其中微服务项目，又分为2个 maven 模块：api, service

#### isass-core

isass-core 是 isass 的底层核心代码，与业务无关，主要封装了常用的框架，工具等，主要有 mq, net, web, database等

#### isass-api

当有2个微服务循环依赖时，就需要把其中1个微服务的被另1个微服务用到的 api 放在 isass-api
#### 微服务项目

1个具体的微服务项目内部一般包括2个 maven 模块：api, service

- api

微服务的实体定义，事件定义，缓存服务，微服务对外接口，rpc 实现等

- service

用于编写业务逻辑和 controller，程序员大部分时间，都和 service 打交道。

### 2选1启动

由于不同企业的发展阶段不同，有些资金雄厚，可以提供大量服务器以支撑微服务的运行；有些资金紧张，只能提供1，2台服务器，此时就不适合微服务部署。

> isass 通过 spring 的 bean 加载机制实现支持自主选择 `单体` `微服务` 2种方式启动。

#### 微服务启动
例如有 `商品微服务`, `订单微服务`, `消息微服务`, `会员微服务`, `商城微服务`

启动 service 模块的 {ms}App, 默认是微服务方式启动，每个微服务独立部署，拥有独立进程。微服务之间通讯通过rpc(默认是feign)通信

#### 单体启动

如果采用单体方式部署，则可以以 `商城微服务` 为主，在 `商城微服务` 的 service pom 中依赖其他几个微服务的 service，并在 `商城微服务` 的配置文件配置 `start.microservice.enabled=false`，启动 service 模块的 {ms}App 即可单体启动，所有微服务都在同一个进程内，微服务之间通讯直接通过进程内部本地调用。

> 这样就可以在开发阶段，以微服务的标准进行开发，在部署阶段，根据企业情况选择1种方式部署。

## 硬件架构
