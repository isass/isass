# 下拉框选项

isass 支持友好的下拉框选项，前端可直接调用接口，获取下拉框内容。

::: tip 示例解释
本章节使用 `课程(Course)`【`语文(CHINESE)`、`数学(MATHEMATICS)`】作为示例
:::

## 前端调用方式

GET /{ms}-service/selectOptions/{key}

* {ms}：提供下拉框选项的微服务
* {key}：下拉框内容的标识

---

## 响应内容

``` json
{
  "success": true,
  "status": 200,
  "message": "操作成功",
  "data": [
    {
      "name": "语文",
      "value": "CHINESE"
    },
    {
      "name": "数学",
      "value": "MATHEMATICS"
    }
  ]
}
```

---

## 概要设计

### 下拉框选项后端实现分类

isass 将下拉框选项实现分为以下两类

- 硬编码下拉框选项
- 数据库参数配置下拉框选项

---

### 适用场景

#### 硬编码下拉框选项
- 适用于选项值比较固定，用户、运营、运维人员均不会根据业务随时调整下拉框选项；
- 后端代码层面需要对下拉框中的多数选项值做业务逻辑判断

#### 数据库参数配置下拉框选项
- 选项值会根据业务随时调整

---

### 优缺点

| | 硬编码下拉框选项 | 数据库参数配置下拉框选项 |
|----|-----------|-----------|
| 优点 | 后端代码层面可直接对下拉框选项对象值进行逻辑判断，无需添加魔法值 | 业务人员可以随时调整选项值 | 
| 缺点 | 需要编程。但是也只需要实现 HardCodeSelectOptionService 接口；<br>无法动态编辑选项值 | 后端如果需要对选项值做业务逻辑，则需要把选项值的 value 内容写在业务逻辑中。<br>而且后端开发需要考虑业务人员对选项值的增删改查做兼容，避免选项值被删除，后端代码报空指针异常等情况。 | 

## 实现方法

### 硬编码下拉框选项

- 添加 CourseSelectOptionService 类，实现 HardCodeSelectOptionService 接口，在类上添加 @Service 的 spring 注解
- 实现以下两个方法

``` java
@Service
public class CourseSelectOptionService implements HardCodeSelectOptionService<String> {

    @Override
    public String getKey() {
        return "course";
    }

    @Override
    public List<SelectOption<String>> getSelectOptions() {
        return CollUtil.newArrayList(
                new SelectOption<String>().setName("语文").setValue("CHINESE"),
                new SelectOption<String>().setName("数学").setValue("MATHEMATICS")
        );
    }

}

```

- 添加 {ms}SelectOptionController。若已有此 controller，则不必重复添加

``` java

@RestController
@Api(tags = "下拉框选项")
public class {ms}SelectOptionController {

    @Resource
    private SelectOptionServiceManager selectOptionServiceManager;

    @SuppressWarnings({"unchecked", "rawtypes"})
    @GetMapping("/{ms}/selectOptions/{key}")
    @ApiOperation("获取{ms}下拉框选项")
    public Resp<List<SelectOption>> findSelectOptions(@PathVariable("key") String key) {
        ISelectOptionService optionService = selectOptionServiceManager.getSelectOptionServices().get(key);
        if (optionService == null) {
            throw new AbsentException(StrUtil.format("selectOption[{}]", key));
        }
        return Resp.bizSuccess(optionService.getSelectOptions());
    }

}

```

::: tip 推荐使用枚举创建 SelectOption
创建 Course 的枚举，可在业务代码中方便调用枚举值
:::

``` java
public enum CourseEnum {

    CHINESE("语文"),
    MATHEMATICS("数学");

    private final String name;

    Type(String name) {
        this.name = name;
    }

}

@Service
public class CourseSelectOptionService implements HardCodeSelectOptionService<String> {

    @Override
    public String getKey() {
        return "course";
    }

    @Override
    public List<SelectOption<String>> getSelectOptions() {
        return Arrays.stream(CourseEnum.values())
                .map(t -> new SelectOption<String>().setName(t.getName()).setValue(t.toString()))
                .collect(Collectors.toList());
    }

}
```

### 数据库参数配置下拉框选项

`数据库参数配置下拉框选项` 复用`基础微服务`的`业务参数配置`模块，前端直接调用基础微服务的接口即可获取选项

- GET /base-service/selectOptions/{key}

下拉框的增删改操作，通过`基础微服务`的`业务参数配置`模块实现，后端无需再编码。