# 资源规划

> 本文将介绍我们的服务应该如何在各个 devops 工具中创建对应目录资源

::: tip 总体原则
按照 isass 服务分层模型，在对应的工具中创建目录。
:::

## rancher

创建以下项目
- 基础通用服务
- 行业通用服务
- 中间件服务