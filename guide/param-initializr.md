# 业务参数初始化

当我们需要在新的环境部署微服务时，经常需要初始化一些业务参数，把这些参数保存到数据库中，以便系统正常运行。传统的做法，是开发人员编写初始化sql脚本，给到部署的同事执行脚本。这样手工的操作，会增大部署成本和风险。随着系统的迭代，初始化脚本越来越混乱，无法做到简单高效的管理。isass 依托 `基础微服务(base-service)` 提供 `业务参数初始化` 的功能，帮助开发者进行参数初始化的工作。

## 使用方法

实现 `ParamInitializer` 接口即可

``` java

import cn.hutool.core.collection.CollUtil;
import org.springframework.stereotype.Component;
import vip.isass.api.service.base.param.initializer.ParamInitializer;
import vip.isass.base.api.model.dto.ParamSingleCodeValueDto;

import java.util.List;

/**
 * 课程参数初始化
 */
@Component
public class CourseParamInitializer implements ParamInitializer {

    @Override
    public String getKey() {
        return "course";
    }

    @Override
    public List<ParamSingleCodeValueDto<?>> getInitializationCodeValues() {
        return CollUtil.newArrayList(
                new ParamSingleCodeValueDto<String>().setCode("语文").setValue("CHINESE"),
                new ParamSingleCodeValueDto<String>().setCode("数学").setValue("MATHEMATICS")
        );
    }

    @Override
    public boolean override() {
        return false;
    }

}

```

### 接口方法解析

::: tip String getKey()

业务参数有key，需要全局唯一。

:::

::: tip List&lt;ParamSingleCodeValueDto<?>> getInitializationCodeValues()

初始化参数的内容。

:::

::: tip boolean override()

微服务重启时，是否强制覆盖内容，即重新将业务参数写到数据库。

:::

### 重启微服务，业务参数即可保存到数据库

<img :src="$withBase('/img/save-param.jpg')" alt="">