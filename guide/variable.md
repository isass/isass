# 变量说明

* {package}: 包名前缀，例如: `com.alibaba`、 `vip.isass`
* {com}: compony 公司名，例如: `alibaba`、 `isass`
* {ms}: microservices 微服务，例如权限微服务：`auth`

::: tip 注意
isass 全部文档中涉及到了上述变量的，请在操作过程中，替换成您公司架构人员定义好的名称
:::
