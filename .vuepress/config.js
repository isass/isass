module.exports = {
    port: "3000",
    dest: "docs",
    ga: "UA-85414008-1",
    base: "/",
    markdown: {
        externalLinks: {
            target: '_blank', rel: 'noopener noreferrer'
        }
    },
    locales: {
        "/": {
            lang: "zh-CN",
            title: "isass微服务框架文档",
            description: "为微服务而生"
        }
    },
    head: [["link", { rel: "icon", href: `/favicon.png` }]],
    themeConfig: {
        repo: "https://gitee.com/isass/super",
        docsRepo: "https://gitee.com/isass/isass",
        editLinks: true,
        locales: {
            "/": {
                label: "简体中文",
                selectText: "Languages",
                editLinkText: "在 GitHub 上编辑此页",
                lastUpdated: "上次更新",
                nav: [
                    {
                        text: "指南",
                        link: "/guide/"
                    }
                ],
                sidebar: {
                    "/guide/": genGuideSidebar(true)
                }
            }
        }
    }
};

function genGuideSidebar(isZh) {
    return [
        {
            title: isZh ? "快速入门" : "Getting Start",
            collapsable: false,
            children: ["", "architecture", "variable", "quick-start", "system-config"]
        },
        {
            title: isZh ? "框架功能" : "Core",
            collapsable: false,
            children: ["criteria.md", "v1code.md", "framework/socketio"]
        },
        {
            title: isZh ? "约定规范" : "contract specification",
            collapsable: false,
            children: ["api-data-format.md"]
        },
        {
            title: isZh ? "基础微服务" : "bisic microservice",
            collapsable: false,
            children: ["param.md", "select-option", "param-initializr", "dict"]
        },
        {
            title: isZh ? "附件微服务" : "attachment microservice",
            collapsable: false,
            children: []
        },
        {
            title: isZh ? "服务部署" : "automation deployment",
            collapsable: false,
            children: ["cicd.md", "cicd-install.md", "cicd-resource-planning.md", "cicd-guide.md"]
        },
        {
            title: isZh ? "升级记录" : "upgrade",
            collapsable: false,
            children: ["super.3.1.1-SNAPSHOT.md", "super.3.2.1-SNAPSHOT.md", "super.3.3.0-SNAPSHOT.md"]
        },
        {
            title: isZh ? "演进计划" : "roadmap",
            collapsable: false,
            children: ["roadmap/2024"]
        }
    ]
}

function genConfigSidebar(isZh) {
    return [
        {
            title: isZh ? "配置" : "Config",
            collapsable: false,
            children: ["", "generator-config"]
        }
    ]
}
