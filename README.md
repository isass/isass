---
home: true
heroImage: /img/logo.png
actionText: 快速开始 →
actionLink: /guide/
features:
  - title: 润物无声
    details: 只做增强不做改变，引入它不会对现有工程产生影响，如丝般顺滑。
  - title: 开箱即用
    details: 只需创建好库表，即可生成代码，快速进行 CRUD 操作，从而节省大量时间。
  - title: 丰富功能
    details: 热加载、代码生成、分页、性能分析等功能一应俱全。
footer: GNU GLPL 3.0 | © 2016-2023 isass
---

# 当前最新版本

```xml
<dependency>
    <groupId>vip.isass</groupId>
    <artifactId>super-core</artifactId>
    <version>latest-version</version>
</dependency>
```

<p align="center">
<img :src="$withBase('/img/put-on-record.png')"/>
<a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44190002004781" target="_blank" style="font-weight:bold;font-size:0.8rem;">粤公网安备 44190002004781号</a>
&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
<a href="https://beian.miit.gov.cn/" target="_blank" style="font-weight:bold;font-size:0.8rem;">粤ICP备20052845号-1</a>
</p>
		 
<script>
import { Notification } from 'element-ui'
export default {
  mounted () {
    var xmlHttp = new XMLHttpRequest()
    xmlHttp.open("GET", "https://img.shields.io/maven-central/v/vip.isass/super-core.json", false)
    xmlHttp.send(null)
    var versionInfo = JSON.parse(xmlHttp.responseText).value.replace('v', '')
    var codeNodeList = document.querySelectorAll('code')
    for (var i = 0; i < codeNodeList.length; i++) {
        codeNodeList[i].innerHTML = codeNodeList[i].innerHTML.replace('latest-version', versionInfo)
    }
  }
}
</script>
