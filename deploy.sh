# 远程仓库未有 gh-pages 分支时执行
# yarn install && yarn build && git branch gh-pages && git checkout gh-pages && rm -rf .vuepress .gitignore .travis.yml &&  rm -rf `ls  |egrep -v '(docs/)'` && cp -r ./docs/* ./ && rm -r docs && git add . && git commit -m"deploy" && git push --force --quiet "git@gitee.com:isass/isass-doc.git" gh-pages:gh-pages && git checkout master && git reset --hard

# 已有 gh-pages 分支时执行
yarn install && yarn build && git clone git@gitee.com:isass/isass-doc.git && git checkout gh-pages && cp -r ./docs/* ./isass-doc && cd isass-doc && git add . && git commit -m"deploy" && git push --force --quiet "git@gitee.com:isass/isass-doc.git" gh-pages:gh-pages && cd ../ && git checkout master && git reset --hard